const ERROR = 'error';
const OFF = 'off';

const BASIC_RULES = {
  'no-await-in-loop': ERROR,
  'no-dupe-else-if': ERROR
};
const BEST_PRACTICES_RULES = {
  curly: [ERROR, 'all'],
  'dot-notation': ERROR,
  eqeqeq: ERROR,
  'no-else-return': ERROR,
  'no-script-url': ERROR,
  'no-useless-concat': ERROR
};
const VARIABLE_RULES = {
  'no-use-before-define': [ERROR, { classes: true, functions: true, variables: true }]
};
const STYLISTIC_RULES = {
  'sort-keys': ERROR
};
const ECMASCRIPT6_RULES = {
  'no-duplicate-imports': [ERROR, { includeExports: true }],
  'no-useless-computed-key': [ERROR, { enforceForClassMembers: true }],
  'no-useless-constructor': ERROR,
  'no-var': ERROR,
  'object-shorthand': [ERROR, 'always', {}],
  'prefer-const': ERROR,
  'prefer-destructuring': OFF,
  'prefer-rest-params': ERROR,
  'prefer-spread': ERROR,
  'prefer-template': ERROR
};

/* eslint-disable sort-keys */
const TYPESCRIPT_RULES = {
  'no-use-before-define': OFF,
  '@typescript-eslint/no-use-before-define': [ERROR],
  '@typescript-eslint/no-unused-vars': [ERROR]
};
/* eslint-enable sort-keys */

module.exports = {
  extends: [
    '@react-native-community',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
    'prettier'
  ],
  ignorePatterns: ['/.next'],
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  parserOptions: {
    ecmaFeatures: {
      jsx: true // Allows for the parsing of JSX
    },
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module' // Allows for the use of imports
  },
  plugins: ['import', 'eslint-plugin-import-helpers', 'optimize-regex'],
  rules: {
    // ESLINT
    ...BASIC_RULES,
    ...BEST_PRACTICES_RULES,
    ...VARIABLE_RULES,
    ...STYLISTIC_RULES,
    ...ECMASCRIPT6_RULES,
    // PLUGIN-IMPORT-HELPERS
    'import-helpers/order-imports': [
      'warn',
      {
        alphabetize: { ignoreCase: true, order: 'asc' },
        groups: ['module', '/^cajajper/', ['absolute', '/^~/', 'parent', 'sibling', 'index']],
        newlinesBetween: 'always'
      }
    ],
    // IMPORT OVERRIDES
    'import/no-duplicates': ERROR,
    'import/no-unresolved': OFF,
    // OPTIMIZE REGEX
    'optimize-regex/optimize-regex': ERROR,
    // REACT OVERRIDES
    'react/display-name': OFF,
    'react/function-component-definition': [
      ERROR,
      {
        namedComponents: 'arrow-function'
      }
    ],
    'react/jsx-key': [ERROR, { checkFragmentShorthand: true }],
    'react/jsx-max-depth': OFF,
    'react/jsx-no-bind': ERROR,
    'react/jsx-no-literals': ERROR,
    'react/jsx-props-no-spreading': OFF,
    'react/no-array-index-key': ERROR,
    'react/no-string-refs': [ERROR, { noTemplateLiterals: true }],
    'react/prop-types': OFF,
    ...TYPESCRIPT_RULES
  },
  settings: {
    react: {
      version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
    }
  }
};
