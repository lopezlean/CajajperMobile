module.exports = {
  plugins: [
    [
      'module:react-native-dotenv',
      {
        moduleName: 'react-native-dotenv'
      },
      'react-native-reanimated/plugin'
    ],
    [
      'module-resolver',
      {
        alias: {
          '~': './src'
        },
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        root: ['./']
      }
    ]
  ],
  presets: [
    'module:metro-react-native-babel-preset',
    ['@babel/preset-env', { targets: { node: 'current' } }]
  ]
};
