import { useNavigation } from '@react-navigation/core';
import React, { useCallback } from 'react';
import { Share } from 'react-native';

import FeaturedNews from './FeaturedNews';
import { NewsItemInterface } from './NewsItemInterface';
import NewsItemList from './NewsItemList';

const NewsItem = ({
  item,
  featured
}: {
  item: NewsItemInterface;
  featured?: boolean;
}): React.ReactElement => {
  const navigation = useNavigation();
  const openUrl = useCallback(() => {
    navigation.navigate('NewsViewScreen', { item });
  }, [item, navigation]);

  const share = useCallback(() => {
    Share.share({
      message: item.url,
      title: 'Caja de Jubilaciones y Pensiones de Entre Ríos'
    });
  }, [item.url]);

  return featured ? (
    <FeaturedNews item={item} onShare={share} onOpenUrl={openUrl} />
  ) : (
    <NewsItemList item={item} onShare={share} onOpenUrl={openUrl} />
  );
};

export default NewsItem;
