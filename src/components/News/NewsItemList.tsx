import React, { useCallback } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { SharedElement } from 'react-navigation-shared-element';

import Button from '../Button';
import BodyMedium from '../typography/body-medium';
import BodySmall from '../typography/body-small';
import { NewsItemInterface } from './NewsItemInterface';
import { BODY_MEDIUM } from '~/constants/Typography';

const styles = StyleSheet.create({
  actions: {
    flex: 1
  },
  actionsContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingTop: 5
  },
  container: {
    marginTop: 20
  },
  containerRow: {
    flexDirection: 'row'
  },
  description: {
    flexDirection: 'column'
  },
  descriptionContainer: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#e0e0e0'
  },
  lastButton: {
    marginLeft: 10
  },
  image: {
    borderRadius: 20,
    width: 80,
    height: 80
  },
  imageContainer: {
    marginRight: 10
  },
  title: {
    ...BODY_MEDIUM,
    fontWeight: 'bold'
  }
});

const NewsItemList = ({
  item,
  onShare,
  onOpenUrl
}: {
  item: NewsItemInterface;
  onShare: () => void;
  onOpenUrl: () => void;
}): React.ReactElement => {
  return (
    <View style={styles.container}>
      <View style={styles.containerRow}>
        <View style={styles.imageContainer}>
          <SharedElement id={`item.${item.id}.image_url`}>
            <Image source={{ uri: item.image }} style={styles.image} resizeMode="cover" />
          </SharedElement>
        </View>
        <View style={styles.descriptionContainer}>
          <View style={styles.description}>
            <SharedElement id={`item.${item.id}.title`}>
              <BodyMedium numberOfLines={2}>{item.title}</BodyMedium>
            </SharedElement>
          </View>
          <View style={styles.actionsContent}>
            <Button type={'secondary'} small={true} icon="navigate-outline" onPress={onOpenUrl}>
              {'Leer más...'}
            </Button>
            <Button
              type={'contrast'}
              iconOnly
              small={true}
              icon="share-outline"
              onPress={onShare}
              style={styles.lastButton}
            >
              {'Compartir...'}
            </Button>
          </View>
        </View>
      </View>
    </View>
  );
};

export default NewsItemList;
