import { gql, useQuery } from '@apollo/client';
import React from 'react';
import { View } from 'react-native';

import { Text } from '../Text';
import NewsItem from './NewsItem';
import { NewsItemInterface } from './NewsItemInterface';
import { NewsLoading } from './NewsLoading';

const LAST_NEWS = gql`
  {
    lastNews {
      id
      title
      image
      body
      url
    }
  }
`;

const News = (): React.ReactElement => {
  const { data, error, loading } = useQuery(LAST_NEWS);

  if (loading) {
    return <NewsLoading repeat={3} />;
  }

  if (error) {
    return <Text>{`Error ${error.message}`}</Text>;
  }
  const ITEMS = data.lastNews.map((item: NewsItemInterface, index: number) => (
    <NewsItem key={item.id} item={item} featured={index === 0} />
  ));
  return <View>{ITEMS}</View>;
};

export default News;
