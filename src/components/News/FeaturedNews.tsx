import * as React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { SharedElement } from 'react-navigation-shared-element';

import BodyMedium from '../typography/body-medium';
import { NewsItemInterface } from './NewsItemInterface';
import Button from '~/components/Button';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: 200,
    overflow: 'hidden',
    borderRadius: 20
  },
  image: {
    width: '100%',
    height: 300
  },
  textContainer: {
    position: 'absolute',
    bottom: 10,
    right: 10
  },
  text: {
    textAlign: 'right',
    backgroundColor: 'rgba(0,0,0,0.7)',
    color: '#fff',
    padding: 8,
    borderRadius: 10,
    overflow: 'hidden'
  },
  actionsContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingTop: 5
  },
  lastButton: {
    marginLeft: 10
  }
});

interface FeaturedNewsProps {
  item: NewsItemInterface;
  onShare: () => void;
  onOpenUrl: () => void;
}

const FeaturedNews = ({ item, onShare, onOpenUrl }: FeaturedNewsProps): React.ReactElement => {
  return (
    <View style={styles.container}>
      <SharedElement id={`item.${item.id}.image_url`}>
        <Image source={{ uri: item.image }} style={styles.image} resizeMode="cover" />
      </SharedElement>
      <View style={styles.textContainer}>
        <BodyMedium numberOfLines={2} fontWeight={'bold'} style={styles.text}>
          {item.title}
        </BodyMedium>
        <View style={styles.actionsContent}>
          <Button type={'secondary'} small={true} icon="navigate-outline" onPress={onOpenUrl}>
            {'Leer más...'}
          </Button>
          <Button
            type={'contrast'}
            iconOnly
            small={true}
            icon="share-outline"
            onPress={onShare}
            style={styles.lastButton}
          >
            {'Compartir...'}
          </Button>
        </View>
      </View>
    </View>
  );
};

export default FeaturedNews;
