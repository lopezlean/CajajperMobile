export interface NewsItemInterface {
  id: string;
  title: string;
  image: string;
  body: string;
  url: string;
}
