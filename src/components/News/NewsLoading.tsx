import React from 'react';
import { View, StyleSheet } from 'react-native';
import * as Svg from 'react-native-svg';
import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient';

import Layout from '~/constants/Layout';

const styles = StyleSheet.create({
  container: {
    position: 'relative'
  }
});

export const NewsLoading = ({ repeat }: { repeat: number }): React.ReactElement => {
  const render = (k: React.Key | null | undefined) => (
    <View key={k} style={styles.container}>
      <SvgAnimatedLinearGradient height={280} width={Layout.window.width}>
        <Svg.Rect x="0" y="0" rx="4" ry="4" width={Layout.window.width} height="200" />
        <Svg.Rect x="0" y="210" rx="4" ry="4" width={Layout.window.width} height="30" />
      </SvgAnimatedLinearGradient>
    </View>
  );
  if (repeat) {
    const m = [];
    for (let index = 0; index <= repeat; index++) {
      m.push(render(index));
    }
    return <>{m}</>;
  }
  return render(0);
};
