import * as React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';

interface ContentProps {
  children: React.ReactElement | React.ReactNode;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10
  },
  contentContainer: {
    flex: 1
  }
});

const Content = ({ children, ...props }: ContentProps): React.ReactElement => {
  return (
    <View style={styles.container} {...props}>
      <ScrollView style={styles.contentContainer}>{children}</ScrollView>
    </View>
  );
};

export default Content;
