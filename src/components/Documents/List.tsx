import React from 'react';
import { List as PaperList } from 'react-native-paper';

import ListItem from './ListItem';

const List = (props) => {
  const items = props.items.map((item) => (
    <ListItem key={item.id} item={item} onPress={props.onItemPress} />
  ));
  return (
    <PaperList.Section>
      <PaperList.Subheader>{props.title || 'Expedientes'}</PaperList.Subheader>
      {items}
    </PaperList.Section>
  );
};

export default List;
