import { gql, useQuery } from '@apollo/client';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useRecoilState } from 'recoil';

import { Text } from '../Text';
import BodyLarge from '../typography/body-large';
import { ExpedienteLoading } from './DocumentLoading';
import List from './List';
import { documentSearchTextState } from './State/SearchState';

const BUSCAR_EXPEDIENTES = gql`
  query documentSearch($q: Int!) {
    documentSearch(query: $q) {
      id
      applicant
      applicants {
        first_name
        last_name
      }
      office {
        name
      }
      last_movements {
        office {
          name
        }
        modified
      }
      benefit_type {
        name
      }
    }
  }
`;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listContainer: {
    flex: 1
  },
  searchPreview: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  searchPreviewText: {
    color: 'gray',
    textAlign: 'center'
  }
});

const Search = ({ searchText }: { searchText: string }): React.ReactElement => {
  const { data, error, loading } = useQuery(BUSCAR_EXPEDIENTES, {
    variables: { q: Number(searchText) }
  });

  if (loading) {
    return <ExpedienteLoading repeat={5} title="BUSCANDO" />;
  }

  if (error) {
    console.log({ error });
    return <Text>{'Error'}</Text>;
  }

  return (
    <View style={styles.listContainer}>
      <List title="RESULTADOS" items={data.documentSearch} />
    </View>
  );
};

const SearchPreview = () => (
  <View style={styles.searchPreview}>
    <Icon name="search" size={32} color={'gray'} />
    <BodyLarge style={styles.searchPreviewText}>
      {'Ingresa un termino de búsqueda... \n'}
      {'Puedes buscar por NUC o por documento'}
    </BodyLarge>
  </View>
);

const SearchSwitcher = (): React.ReactElement => {
  const [documentSearchText] = useRecoilState(documentSearchTextState);

  return (
    <View style={styles.container}>
      {documentSearchText ? <Search searchText={documentSearchText} /> : <SearchPreview />}
    </View>
  );
};

export default SearchSwitcher;
