import { gql } from '@apollo/client';
import { useNavigation } from '@react-navigation/core';
import React, { useCallback, useMemo } from 'react';
import { StyleSheet } from 'react-native';
import { List as PaperList, Text } from 'react-native-paper';

import { View } from '../Themed';

const styles = StyleSheet.create({
  item: {
    borderBottomColor: '#e0e0e0',
    borderBottomWidth: 1
  },
  text: {
    fontSize: 14,
    color: 'gray'
  }
});

/*const ADD_TO_FAVORITE = gql`
  mutation addToFavorites($favorites: [Int]!) {
    addToFavorites(favorites: $favorites) {
      id
      favorites {
        id
      }
    }
  }
`;*/

const ListItem = ({ item }): React.ReactElement => {
  const navigation = useNavigation();
  const view = useCallback(() => {
    navigation.navigate('DocumentsView', { item });
  }, [item, navigation]);

  const description = useMemo(() => {
    const getSolicitante = () => {
      if (item.applicants.length >= 1) {
        const fullName = `${item.applicants[0].first_name.trim()} ${item.applicants[0].last_name.trim()}`.trim();
        if (fullName.length === 0) {
          return 'S/N';
        }
        return fullName;
      }
      const ret = item.applicant ? item.applicant.trim() : '';
      if (ret.length === 0) {
        return 'S/N';
      }
      return ret;
    };

    const getTipoBeneficio = () => {
      if (item.benefit_type) {
        return item.benefit_type.name;
      }
    };

    const getArea = () => {
      if (item.area) {
        return item.office.name;
      }
    };
    return (
      <View>
        <Text style={styles.text} numberOfLines={1}>
          {getSolicitante()}
        </Text>
        <Text style={styles.text} numberOfLines={1}>
          {getTipoBeneficio()}
        </Text>
        <Text style={styles.text} numberOfLines={1}>
          {'Ubicación:'} {getArea()}
        </Text>
      </View>
    );
  }, [item.applicant, item.applicants, item.area, item.benefit_type, item.office.name]);

  const left = useCallback(
    (props) => <PaperList.Icon {...props} icon="file-tray-full-outline" />,
    []
  );

  return (
    <PaperList.Item
      style={styles.item}
      title={`NUC: ${item.id}`}
      onPress={view}
      description={description}
      left={left}
    />
  );
};

export default ListItem;
