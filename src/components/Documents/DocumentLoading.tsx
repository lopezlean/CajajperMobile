import React, { useCallback } from 'react';
import { View, StyleSheet } from 'react-native';
import { List as PaperList } from 'react-native-paper';
import * as Svg from 'react-native-svg';
import SvgAnimatedLinearGradient from 'react-native-svg-animated-linear-gradient';

import Layout from '../../constants/Layout';

const styles = StyleSheet.create({
  item: {
    borderBottomColor: '#e0e0e0',
    borderBottomWidth: 1
  }
});

export const ExpedienteLoading = (props): React.ReactElement => {
  const left = useCallback(
    (props) => <PaperList.Icon {...props} icon="file-tray-full-outline" />,
    []
  );

  const item = (k: number) => (
    <PaperList.Item
      key={k}
      style={styles.item}
      title={
        <SvgAnimatedLinearGradient height={18} width={Layout.window.width}>
          <Svg.Rect key={1} x="0" y="0" rx="0" ry="4" width="80%" height="13" />
        </SvgAnimatedLinearGradient>
      }
      description={
        <SvgAnimatedLinearGradient height={31} width={Layout.window.width}>
          <Svg.Rect key={2} x="0" y="0" rx="4" ry="4" width="80%" height="13" />
          <Svg.Rect key={3} x="0" y="18" rx="4" ry="4" width="60%" height="13" />
        </SvgAnimatedLinearGradient>
      }
      left={left}
    />
  );
  const items = [];
  if (props.repeat) {
    for (let index = 0; index <= props.repeat; index++) {
      items.push(item(index));
    }
  } else {
    items.push(item(1));
  }

  return (
    <View>
      <PaperList.Section>
        <PaperList.Subheader>{props.title || 'Expedientes'}</PaperList.Subheader>

        {items}
      </PaperList.Section>
    </View>
  );
};
