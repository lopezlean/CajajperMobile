import { atom } from 'recoil';

// note count
export const documentSearchFocusState = atom({
  default: false,
  key: 'documentSearchFocusState'
});

export const documentSearchTextState = atom({
  default: '',
  key: 'documentSearchTextState'
});
