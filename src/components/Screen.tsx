import * as React from 'react';
import { StyleSheet, ScrollView, SafeAreaView, View } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollView: {
    flexGrow: 1
  }
});

interface ScreenProps {
  children: React.ReactNode | React.ReactElement;
  padder?: boolean;
}

const Screen = ({ children, padder = true }: ScreenProps): React.ReactElement => {
  const padding = { paddingHorizontal: padder ? 10 : 0 };
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        contentContainerStyle={styles.scrollView}
      >
        <View style={[styles.container, padding]}>{children}</View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Screen;
