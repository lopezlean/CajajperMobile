import React, { useRef, useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { Appbar, Searchbar } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { useRecoilState } from 'recoil';

import { documentSearchFocusState, documentSearchTextState } from './Documents/State/SearchState';
import useColorScheme from '~/hooks/useColorScheme';

const styles = StyleSheet.create({
  searchContainer: {
    flex: 1
  }
});

const theme = {
  dark: {
    dark: true,
    colors: {
      text: '#000'
    }
  },
  light: {
    dark: false
  }
};

const AppSearchHeader = (props) => {
  const input = useRef(null);
  const [documentSearchFocus, setDocumentSearchFocus] = useRecoilState(documentSearchFocusState);
  const [documentSearchText, setDocumentSearchText] = useRecoilState(documentSearchTextState);

  const colorScheme = useColorScheme();

  const cancel = useCallback(() => {
    setDocumentSearchText('');
    setDocumentSearchFocus(false);

    if (!input || !input.current || typeof input.current !== 'object') {
      return;
    }
    input.current.value = '';
  }, [setDocumentSearchFocus, setDocumentSearchText]);

  const onFocus = useCallback(() => {
    setDocumentSearchFocus(true);
  }, [setDocumentSearchFocus]);
  const onBlur = useCallback(() => {
    setDocumentSearchFocus(false);
  }, [setDocumentSearchFocus]);

  const onChangeText = useCallback(
    (text) => {
      setDocumentSearchText(text);
    },
    [setDocumentSearchText]
  );
  const onQRClick = useCallback(() => {
    props.navigation.navigate('QrCode');
  }, [props.navigation]);

  return (
    <Appbar.Header>
      <View style={styles.searchContainer}>
        <Searchbar
          theme={theme[colorScheme]}
          placeholder="Buscar expedientes"
          onChangeText={onChangeText}
          ref={input}
          onFocus={onFocus}
          onBlur={onBlur}
          keyboardType="number-pad"
          value={documentSearchText}
          icon="search-outline"
          clearIcon="close-outline"
        />
      </View>
      <Appbar.Action icon={'qr-code-outline'} onPress={onQRClick} />
    </Appbar.Header>
  );
};

export default AppSearchHeader;
