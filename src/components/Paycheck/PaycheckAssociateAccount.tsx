import { gql, useMutation } from '@apollo/client';
import { useNavigation } from '@react-navigation/core';
import * as WebBrowser from 'expo-web-browser';
import React, { useCallback } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet,
  StatusBar,
  Alert
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import { useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

import Button from '~/components/Button';
import LoginForm, { SubmitData } from '~/components/LoginForm';
import BodyMedium from '~/components/typography/body-medium';
import Colors from '~/constants/Colors';
import AuthContext from '~/context/AuthContext';

const RECIBO_DIGITAL_ASSOCIATE = gql`
  mutation paycheckAssociate($email: String!, $password: String!) {
    paycheckAssociate(email: $email, password: $password)
  }
`;

const styles = StyleSheet.create({
  action: {
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 5
  },
  actionError: {
    borderBottomColor: '#FF0000',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 5
  },
  button: {
    marginTop: 50,
    width: '100%'
  },
  container: {
    backgroundColor: Colors.primaryColor,
    flex: 1
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14
  },
  footer: {
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex: 3,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 50,
    paddingHorizontal: 20
  },
  lastButton: {
    marginTop: 10
  },
  signIn: {
    alignItems: 'center',
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    width: '100%'
  },
  textInput: {
    color: '#05375a',
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10
  },

  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  text_header: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }
});

const PaycheckAssociateAccount = (): React.ReactElement => {
  const { colors } = useTheme();
  const navigation = useNavigation();

  const { signIn } = React.useContext(AuthContext);
  const [loginMutation] = useMutation(RECIBO_DIGITAL_ASSOCIATE);

  const goToRegister = useCallback(() => {
    WebBrowser.openBrowserAsync('https://recibodigital.cajajper.gov.ar/register.php');
  }, []);

  const loginHandle = useCallback(
    async (data: SubmitData) => {
      try {
        const result = await loginMutation({
          variables: { email: data.username, password: data.password }
        });
        console.log({ result });
        if (result.data.paycheckAssociate) {
          // navigate back and pass parameters
          console.log('GOOOO');
          navigation.goBack();
        } else {
          Alert.alert('Error!', 'Nombre de usuario o contraseñas incorrectos.', [{ text: 'OK' }]);
        }
      } catch (e) {
        Alert.alert('Error!', 'Nombre de usuario o contraseñas incorrectos.', [{ text: 'OK' }]);
      }
    },
    [loginMutation, signIn]
  );

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primaryColor} barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>{'Asociar cuenta de Recibo digital'}</Text>
      </View>
      <Animatable.View
        animation="fadeInUpBig"
        style={[
          styles.footer,
          {
            backgroundColor: colors.background
          }
        ]}
      >
        <LoginForm onSubmit={loginHandle} />

        <Button type="outline" style={[styles.button, styles.lastButton]} onPress={goToRegister}>
          {'Registrarse'}
        </Button>
      </Animatable.View>
    </View>
  );
};

export default PaycheckAssociateAccount;
