import { gql, useQuery } from '@apollo/client';
import { useNavigation } from '@react-navigation/core';
import moment from 'moment';
import * as React from 'react';
import { useCallback, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { NavigationStackProp } from 'react-navigation-stack';
import { useRecoilValue } from 'recoil';

import Button from '../Button';

const PaycheckMontView = ({ children }: { children: string }): React.ReactElement => {
  const navigation = useNavigation<NavigationStackProp>();
  return (
    <Button icon="menu-outline" type="secondary" small={true} onPress={navigation.toggleDrawer}>
      {children}
    </Button>
  );
};

export default PaycheckMontView;
