import { gql, useQuery } from '@apollo/client';
import { useNavigation } from '@react-navigation/core';
import React, { useRef, useCallback, useMemo } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import Content from '../Content';
import EmptyMessage from '../EmptyMessage';
import Search from '../Documents/Search';
import { Text } from '../Text';
import PaycheckListItemInterface from './Interfaces/PaycheckListItemInterface';
import RecibosDigitalesAccountSelector from './PaycheckAccountSelector';
import SelectPaycheckButton from './SelectPaycheckButton';

const ME = gql`
  query myInfo {
    myInfo {
      id
      paycheck_users {
        ids
        users {
          id
          national_identification_number
          full_account_number
          full_name
          email
        }
      }
    }
  }
`;

const styles = StyleSheet.create({
  container: {
    marginTop: 5
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

interface ReciboDigitalesListInterface {
  item: PaycheckListItemInterface | null | undefined;
}

const RecibosDigitalesList = ({ item }: ReciboDigitalesListInterface): React.ReactElement => {
  const router = useNavigation();
  const { data, error, loading } = useQuery(ME);

  const onReciboDigitalAssociateClick = useCallback(() => {
    router.navigate('PaycheckAssociateAccount');
  }, []);
  const renderEmpty = useMemo(() => {
    return (
      <EmptyMessage
        text="Aún no tienes una cuenta asociada"
        icon={'wallet-outline'}
        button={{
          onPress: onReciboDigitalAssociateClick,
          text: 'Asociar cuenta'
        }}
      />
    );
  }, [onReciboDigitalAssociateClick]);

  const renderAccounts = useMemo(() => {
    if (loading || error) {
      return;
    }
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <SelectPaycheckButton icon>
            {item ? `Recibo ${`0${item.month}`.slice(-2)}/${item.year}` : 'Seleccionar...'}
          </SelectPaycheckButton>
          {data.myInfo.paycheck_users && (
            <RecibosDigitalesAccountSelector items={data.myInfo.paycheck_users.users} />
          )}
        </View>
      </View>
    );
  }, [data, error, loading, item]);

  if (loading) {
    return <Text>{'Cargando'}</Text>;
  }

  if (error) {
    return <Text>{`Error ${error.message}`}</Text>;
  }
  const ITEMS = data.myInfo && data.myInfo.paycheck_users ? renderAccounts : renderEmpty;

  return <View style={styles.container}>{ITEMS}</View>;
};

export default RecibosDigitalesList;
