export default interface PaycheckUserInterface {
  id: number;
  national_identification_number: number;
  full_account_number: string;
  full_name: string;
  email: string;
}
