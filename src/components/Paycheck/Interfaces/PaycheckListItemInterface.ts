export default interface PaycheckListItemInterface {
  id: number;
  account_number: number;
  full_account_number: string;
  month: number;
  year: number;
  type: string;
  file_type_id: number;
  annex_id: number;
}
