import PaycheckListItemInterface from './PaycheckListItemInterface';

export default interface PaycheckListInterface {
  year: number;
  month: number;
  paycheck: PaycheckListItemInterface[];
}
