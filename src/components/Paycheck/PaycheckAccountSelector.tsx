import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { useRecoilState } from 'recoil';

import BodySmall from '../typography/body-small';
import { payckeckUserState } from './State/PaycheckUserState';
import Picker from '~/components/Picker';
import { BODY_SMALL } from '~/constants/Typography';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  selector: {
    ...BODY_SMALL
  }
});

const PickerWrapper = ({ items }) => {
  const [selectedValue, setSelectedValue] = useRecoilState(payckeckUserState);
  const onValueChange = useCallback(
    (item) => {
      setSelectedValue(item);
    },
    [setSelectedValue]
  );

  const parsedItems = useMemo(() => {
    return items.map((item, index) => {
      return {
        label: item.email,
        value: item
      };
    });
  }, [items]);

  useEffect(() => {
    if (!selectedValue) {
      setSelectedValue(items[0]);
    }
  }, [items, selectedValue, setSelectedValue]);

  return (
    <Picker onValueChange={onValueChange} items={parsedItems} value={selectedValue}>
      {'Seleccionar cuenta...'}
    </Picker>
  );
};
const RecibosDigitalesAccountSelector = ({ items }): React.ReactElement => {
  return (
    <View style={styles.container}>
      <BodySmall>{'Cuenta: '}</BodySmall>
      <PickerWrapper items={items} />
    </View>
  );
};

export default RecibosDigitalesAccountSelector;
