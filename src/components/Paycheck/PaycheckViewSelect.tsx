import { useNavigation } from '@react-navigation/core';
import * as React from 'react';
import { useCallback, useEffect, useMemo } from 'react';
import { Text, View, StyleSheet } from 'react-native';

import Button from '../Button';
import EmptyMessage from '../EmptyMessage';
import PaycheckListItemInterface from './Interfaces/PaycheckListItemInterface';

const styles = StyleSheet.create({
  button: {
    marginTop: 5,
    alignSelf: 'center'
  },
  buttonsContainer: {
    marginTop: 5
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  }
});

interface PaycheckViewSelectProps {
  item: PaycheckListItemInterface[] | null;
}

const PaycheckViewSelectButton = ({ paycheck }: { paycheck: PaycheckListItemInterface }) => {
  const navigation = useNavigation();
  const onPress = useCallback(() => {
    navigation.navigate('Paycheck', { item: paycheck });
  }, [paycheck, navigation]);

  return (
    <Button type={'primary'} style={styles.button} onPress={onPress}>
      {paycheck.type}
    </Button>
  );
};

const PaycheckViewSelect = ({ item }: PaycheckViewSelectProps): React.ReactElement => {
  const navigation = useNavigation();
  const selector = useMemo(() => {
    if (!item) {
      return null;
    }
    return item.map((paycheck) => (
      <PaycheckViewSelectButton key={paycheck.type} paycheck={paycheck} />
    ));
  }, [item]);

  useEffect(() => {
    if (!item) {
      return;
    }
    console.log({ l: item.length });
    if (item?.length === 1) {
      navigation.navigate('Paycheck', { item: item[0] });
    }
  }, [item, navigation]);

  return (
    <View style={styles.container}>
      <EmptyMessage text="Selecciona un recibo:" icon="wallet-outline">
        <View style={styles.buttonsContainer}>{selector}</View>
      </EmptyMessage>
    </View>
  );
};

export default PaycheckViewSelect;
