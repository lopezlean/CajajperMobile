import { useNavigation } from '@react-navigation/core';
import moment from 'moment';
import React, { memo, useCallback, useMemo } from 'react';
import { StyleSheet } from 'react-native';
import { List } from 'react-native-paper';

import PaycheckListInterface from '../Interfaces/PaycheckListInterface';
import PaycheckListItemInterface from '../Interfaces/PaycheckListItemInterface';

interface buildPaycheckProps {
  items: PaycheckListInterface[];
}

const styles = StyleSheet.create({
  listItemTitle: {
    paddingLeft: 80,
    textAlign: 'left',
    textTransform: 'capitalize'
  }
});

const ListIcon = (props: unknown) => <List.Icon {...props} icon="folder-outline" />;
const ListItemGroupIcon = (props: unknown) => (
  <List.Icon {...props} icon="ellipsis-horizontal-outline" />
);

const ListItem = ({ item }: { item: PaycheckListItemInterface[] }): React.ReactElement => {
  const navigation = useNavigation();
  const onPress = useCallback(() => {
    navigation.navigate('Paycheck', { item });
  }, [item, navigation]);
  const date = new Date();
  const monthName = moment(date.setMonth(item[0].month - 1)).format('MMMM');
  return (
    <List.Item
      title={`${monthName}`}
      onPress={onPress}
      titleStyle={styles.listItemTitle}
      right={item.length > 1 ? ListItemGroupIcon : undefined}
    />
  );
};

const BuildPaycheckList = memo(
  ({ items }: buildPaycheckProps): React.ReactElement => {
    const itemsMap = useMemo(() => {
      const ret: React.ReactElement[] = [];
      let currentYear: number | null = null;
      let currentYearItems: React.ReactElement[] = [];
      items.forEach((element) => {
        if (!currentYear) {
          currentYear = element.year;
        } else {
          if (currentYear !== element.year) {
            ret.push(
              <List.Accordion title={currentYear} key={currentYear} left={ListIcon}>
                {currentYearItems}
              </List.Accordion>
            );
            currentYearItems = [];
            currentYear = element.year;
          }
        }

        currentYearItems.push(
          <ListItem
            theme="dark"
            key={`paychecklistitem-${element.month}${element.year}`}
            item={element.paycheck}
          />
        );
      });
      ret.push(
        <List.Accordion title={currentYear} key={currentYear} left={ListIcon}>
          {currentYearItems}
        </List.Accordion>
      );
      return ret;
    }, [items]);

    return <List.Section title="Recibos">{itemsMap}</List.Section>;
  }
);
export default BuildPaycheckList;
