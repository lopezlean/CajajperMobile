import { gql, useLazyQuery, useQuery } from '@apollo/client';
import { DrawerItem, DrawerContentScrollView } from '@react-navigation/drawer';
import React, { useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Avatar, Title, Caption, Drawer, Text, TouchableRipple } from 'react-native-paper';
import { useRecoilValue } from 'recoil';

import PaycheckUserInterface from '../Interfaces/PaycheckUserInterface';
import { payckeckUserState } from '../State/PaycheckUserState';
import BuildPaycheckList from './BuildPaycheckList';
import Icon, { IconSizeType } from '~/components/Icon';
import BodyLarge from '~/components/typography/body-large';

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1
  },
  userInfoSection: {
    paddingLeft: 20
  },
  title: {
    marginTop: 20,
    fontWeight: 'bold'
  },
  caption: {
    fontSize: 14,
    lineHeight: 14
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3
  },
  drawerSection: {
    marginTop: 15
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16
  }
});

const PAYCHECK_LIST = gql`
  query paycheckList($account_id: Int!) {
    paycheckList(account_id: $account_id) {
      year
      month
      paycheck {
        account_number
        full_account_number
        month
        year
        type
        file_type_id
        annex_id
      }
    }
  }
`;

const PaycheckNavigationDrawer = (props) => {
  //console.log(props.state.routes[0].name);
  const user = useRecoilValue<PaycheckUserInterface>(payckeckUserState);

  const { index, routes } = props.state;
  const currentRouteName = routes[index].name;
  const [expanded, setExpanded] = useState(true);
  const [items, setItems] = useState([]);

  const [getPaycheckList, { error, loading }] = useLazyQuery(PAYCHECK_LIST, {
    onCompleted: (d) => {
      setItems(d.paycheckList);
    }
  });

  const handlePress = () => setExpanded(!expanded);
  useEffect(() => {
    if (!user) {
      return;
    }
    getPaycheckList({
      variables: { account_id: Number(user.id) }
    });
  }, [user, getPaycheckList]);

  if (!user) {
    return <Text>{'No hay cuenta seleccionada'}</Text>;
  }

  if (loading || !items || !Array.isArray(items)) {
    return <Text>{'Cargando'}</Text>;
  }

  if (error) {
    return <Text>{`Error ${error.message} ${user.id}`}</Text>;
  }

  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
          <Avatar.Text label="RD" size={50} />
          <Title style={styles.title}>
            <BodyLarge>
              {items && items.paycheckList && items.paycheckList[0] && items.paycheckList[0].year}
            </BodyLarge>
          </Title>
          <Caption style={styles.caption}>{user.full_account_number}</Caption>
          <Caption style={styles.caption}>{user.full_name}</Caption>
          <Caption style={styles.caption}>{user.national_identification_number}</Caption>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          <BuildPaycheckList items={items} account_id={user.id} />
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="account-outline" color={color} size={size as IconSizeType} />
            )}
            label="Profile"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="tune" color={color} size={size as IconSizeType} />
            )}
            label="Preferences"
            onPress={() => {}}
          />
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="bookmark-outline" color={color} size={size as IconSizeType} />
            )}
            label="Bookmarks"
            onPress={() => {}}
          />
        </Drawer.Section>
        <Drawer.Section title="Opciones">
          <TouchableRipple onPress={() => {}}>
            <View style={styles.preference}>
              <Text>{'Quitar esta cuenta'}</Text>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </View>
    </DrawerContentScrollView>
  );
};

export default PaycheckNavigationDrawer;
