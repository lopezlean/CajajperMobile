import { gql, useLazyQuery, useQuery } from '@apollo/client';
import * as FileSystem from 'expo-file-system';
import * as IntentLauncher from 'expo-intent-launcher';
import * as Linking from 'expo-linking';
import React, { useCallback, useMemo } from 'react';
import { Text, View, StyleSheet, Alert } from 'react-native';
import { DataTable } from 'react-native-paper';
import { useRecoilValue } from 'recoil';

import Button from '../Button';
import EmptyMessage from '../EmptyMessage';
import PaycheckListItemInterface from './Interfaces/PaycheckListItemInterface';
import PaycheckUserInterface from './Interfaces/PaycheckUserInterface';
import PaycheckViewSelect from './PaycheckViewSelect';
import { payckeckUserState } from './State/PaycheckUserState';
import Enviroment from '~/constants/Enviroment';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rowAmount: {
    flex: 1
  },
  rowDescription: {
    flex: 2
  },
  tableFooter: {
    backgroundColor: 'gray'
  },
  tableFooterTitle: {
    color: '#fff'
  },
  userActions: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5
  }
});

interface PaycheckViewProps {
  item: PaycheckListItemInterface[] | PaycheckListItemInterface | null;
}
interface PaycheckViewRenderProps {
  item: PaycheckListItemInterface;
}

const PAYCHECK_VIEW = gql`
  query paycheckView(
    $account_id: Int!
    $year: Int!
    $month: Int!
    $type: String!
    $file_type_id: Int!
    $annex_id: Int!
  ) {
    paycheckView(
      account_id: $account_id
      year: $year
      month: $month
      type: $type
      file_type_id: $file_type_id
      annex_id: $annex_id
    ) {
      id
      year
      amount
      paycheck_code {
        id
        description
        type
      }
      paycheck_annex {
        id
        name
      }
    }
  }
`;

const PAYCHECK_PDF_VIEW = gql`
  query paycheckPdfView(
    $account_id: Int!
    $year: Int!
    $month: Int!
    $type: String!
    $file_type_id: Int!
    $annex_id: Int!
  ) {
    paycheckPdfView(
      account_id: $account_id
      year: $year
      month: $month
      type: $type
      file_type_id: $file_type_id
      annex_id: $annex_id
    )
  }
`;

const PaycheckViewRender = ({ item }: PaycheckViewRenderProps): React.ReactElement => {
  const user: PaycheckUserInterface = useRecoilValue(payckeckUserState) as PaycheckUserInterface;

  const { year, month, type, file_type_id, annex_id } = item;

  const { error, loading, data } = useQuery(PAYCHECK_VIEW, {
    variables: {
      account_id: Number(user.id),
      annex_id,
      file_type_id,
      month,
      type,
      year
    }
  });

  const [getPaycheckPdf] = useLazyQuery(PAYCHECK_PDF_VIEW, {
    onCompleted: (pdfData) => {
      console.log({
        pdfData
      });

      if (!pdfData.paycheckPdfView) {
        Alert.alert('Error', 'Se produjo un error en la solicitud del PDF');
        return;
      }

      try {
        Linking.openURL(Enviroment.staticFilesUrl + pdfData.paycheckPdfView);
      } catch (e) {
        console.error(e);
      }
    },
    variables: {
      account_id: Number(user.id),
      annex_id,
      file_type_id,
      month,
      type,
      year
    }
  });

  const downloadPaycheckPdf = useCallback(() => {
    getPaycheckPdf();
  }, [getPaycheckPdf]);
  const renderedItems = useMemo(() => {
    if (!data || loading || error) {
      return;
    }
    const TOTALS: [number, number, number] = [0, 0, 0];
    const rows: [string, number, number, number][] = data.paycheckView.map((element) => {
      TOTALS[element.paycheck_code.type === 'HABER' ? 0 : 1] += element.amount;
      return [
        element.paycheck_code.description,
        element.paycheck_code.type === 'HABER' ? element.amount : null,
        element.paycheck_code.type !== 'HABER' ? element.amount : null,
        element.id
      ];
    });

    const map = rows.map((row) => (
      <DataTable.Row key={row[3]}>
        <DataTable.Cell style={styles.rowDescription}>{row[0]}</DataTable.Cell>
        <DataTable.Cell style={styles.rowAmount} numeric>
          {row[1]}
        </DataTable.Cell>
        <DataTable.Cell style={styles.rowAmount} numeric>
          {row[2]}
        </DataTable.Cell>
      </DataTable.Row>
    ));
    TOTALS[0] = Math.round((TOTALS[0] + Number.EPSILON) * 100) / 100;
    TOTALS[1] = Math.round((TOTALS[1] + Number.EPSILON) * 100) / 100;
    TOTALS[2] = Math.round((TOTALS[0] + TOTALS[1] + Number.EPSILON) * 100) / 100;

    return (
      <React.Fragment>
        {map}
        <DataTable.Header style={styles.tableFooter}>
          <DataTable.Title style={styles.rowDescription}>
            <Text style={styles.tableFooterTitle}>{'TOTALES'}</Text>
          </DataTable.Title>
          <DataTable.Title style={styles.rowAmount} numeric>
            <Text style={styles.tableFooterTitle}>{`$ ${TOTALS[0]}`}</Text>
          </DataTable.Title>
          <DataTable.Title style={styles.rowAmount} numeric>
            <Text style={styles.tableFooterTitle}>{`$ ${TOTALS[1]}`}</Text>
          </DataTable.Title>
        </DataTable.Header>

        <DataTable.Header style={styles.tableFooter}>
          <DataTable.Title style={styles.rowDescription}>
            <Text style={styles.tableFooterTitle}>{'LIQUIDO'}</Text>
          </DataTable.Title>
          <DataTable.Title style={[styles.rowAmount]} numeric>
            <Text style={styles.tableFooterTitle}>{`$ ${TOTALS[2]}`}</Text>
          </DataTable.Title>
        </DataTable.Header>
      </React.Fragment>
    );
  }, [data, error, loading]);

  if (loading) {
    return <EmptyMessage text="Cargando..." />;
  }
  if (error) {
    return <EmptyMessage text="Ocurrio un error inesperado. Por favor, intente nuevamente." />;
  }

  if (!data.paycheckView || data.paycheckView.length < 1) {
    return (
      <EmptyMessage text="No se encontraron recibos de sueldos para este período. Por favor, selecciona una fecha nueva." />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.userActions}>
        <Button disabled={true} type="secondary" small={true} icon="document-attach-outline">
          {data.paycheckView[0].paycheck_annex.name}
        </Button>
        <Button
          disabled={false}
          type="primary"
          small={true}
          icon="download-outline"
          onPress={downloadPaycheckPdf}
        >
          {'Descargar'}
        </Button>
      </View>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title style={styles.rowDescription}>{'Descripción'}</DataTable.Title>
          <DataTable.Title style={styles.rowAmount} numeric>
            {'Haber'}
          </DataTable.Title>
          <DataTable.Title style={styles.rowAmount} numeric>
            {'Descuento'}
          </DataTable.Title>
        </DataTable.Header>

        {renderedItems}
      </DataTable>
    </View>
  );
};

const PaycheckView = ({ item }: PaycheckViewProps): React.ReactElement => {
  return (
    <View style={styles.container}>
      {item ? (
        Array.isArray(item) ? (
          <PaycheckViewSelect item={item} />
        ) : (
          <PaycheckViewRender item={item} />
        )
      ) : (
        <EmptyMessage text="Selecciona un recibo" icon="wallet-outline" />
      )}
    </View>
  );
};

export default PaycheckView;
