import { atom } from 'recoil';

import PaycheckUserInterface from '~/components/Paycheck/Interfaces/PaycheckUserInterface';

export const payckeckUserState = atom({
  default: undefined as PaycheckUserInterface | undefined,
  key: 'payckeckUserState'
});
