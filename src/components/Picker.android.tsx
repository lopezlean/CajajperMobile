import { Picker as RNPicker } from '@react-native-picker/picker';
import React, { useState, useCallback, useMemo, useEffect, useRef } from 'react';
import { View, StyleSheet } from 'react-native';

import { SMALL_BUTTON } from '~/constants/Typography';

const styles = StyleSheet.create({
  container: {
    width: 150
  },
  picker: {
    height: 10,
    width: 150
  },

  pickerItem: {
    fontSize: SMALL_BUTTON.fontSize
  }
});

interface PickerInterface {
  children: string;
  value: unknown;
  items: {
    value: unknown;
    label: string;
  }[];
  onValueChange: (value: unknown) => null;
}

const Picker = ({ items, children, onValueChange, value }: PickerInterface): React.ReactElement => {
  const [currentItem, setCurrentItem] = useState('');

  const parsedItemsOptions = useMemo(() => {
    return items.map((item) => {
      return item.label;
    });
  }, [items]);

  const parsedItemsMap = useMemo(() => {
    return items.map((item) => {
      return item.value;
    });
  }, [items]);

  const onPress = useCallback(
    (itemValue) => {
      setCurrentItem(itemValue);
      onValueChange(parsedItemsMap[itemValue]);
    },
    [onValueChange, parsedItemsMap]
  );

  useEffect(() => {
    if (value) {
      const index = parsedItemsMap.findIndex((element) => element === value);
      if (index !== undefined) {
        setCurrentItem(parsedItemsOptions[index]);
      }
    }
  }, [parsedItemsMap, parsedItemsOptions, value]);

  const parsedItemsRender = useMemo(() => {
    return items.map((item, index) => {
      return (
        <RNPicker.Item
          style={styles.pickerItem}
          key={item.label}
          label={item.label}
          value={index}
        />
      );
    });
  }, [items]);

  return (
    <View style={styles.container}>
      <RNPicker style={styles.picker} selectedValue={currentItem} onValueChange={onPress}>
        {parsedItemsRender}
      </RNPicker>
    </View>
  );
};

export default Picker;
