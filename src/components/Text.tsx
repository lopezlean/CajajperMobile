import * as React from 'react';

import { Text as ThemedText, TextProps } from './Themed';

export const Text = (props: TextProps): React.ReactElement => (
  <ThemedText {...props} style={[props.style]} />
);
