import { BlurView } from '@react-native-community/blur';
import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});

const Backdrop = (): React.ReactElement => {
  return <BlurView blurType="light" blurAmount={2} style={styles.absolute} />;
};

export default Backdrop;
