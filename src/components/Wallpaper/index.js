import React from 'react';
import { StyleSheet, ImageBackground } from 'react-native';

const Wallpaper = (props) => (
  <ImageBackground style={styles.picture} source={props.source} imageStyle={styles.imageStyle}>
    {props.children}
  </ImageBackground>
);

export default Wallpaper;

const styles = StyleSheet.create({
  imageStyle: {
    resizeMode: 'cover'
  },
  picture: {
    flex: 1,
    height: null,
    width: null
  }
});
