import React, { useCallback } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Ionicons';

import Button from './Button';
import BodyMedium from './typography/body-medium';
import Colors from '~/constants/Colors';

const styles = StyleSheet.create({
  action: {
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 5
  },
  actionError: {
    borderBottomColor: '#FF0000',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 5
  },
  button: {
    marginTop: 50,
    width: '100%'
  },
  container: {},
  errorMsg: {
    color: '#FF0000',
    fontSize: 14
  },
  footer: {
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex: 3,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 50,
    paddingHorizontal: 20
  },

  textInput: {
    color: '#05375a',
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10
  },

  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  text_header: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }
});
export type SubmitData = {
  username: string;
  password: string;
};
interface LoginFormProps {
  onSubmit: ((data: SubmitData) => null) | ((data: SubmitData) => Promise<null>);
}

const LoginForm = ({ onSubmit }: LoginFormProps): React.ReactElement => {
  const [data, setData] = React.useState({
    check_textInputChange: false,
    formIsReady: false,
    isValidPassword: true,
    isValidUser: true,
    password: '',
    secureTextEntry: true,
    username: ''
  });
  const submitHandle = useCallback(() => {
    onSubmit({
      password: data.password,
      username: data.username
    });
  }, [data, onSubmit]);
  const textInputChange = useCallback(
    (val) => {
      const reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
      const isValid = reg.test(val);

      if (isValid) {
        setData({
          ...data,
          check_textInputChange: true,
          formIsReady: data.isValidPassword && data.password.length > 0,
          isValidUser: true,
          username: val
        });
      } else {
        setData({
          ...data,
          check_textInputChange: false,
          formIsReady: false,
          isValidUser: false,
          username: val
        });
      }
    },
    [data]
  );

  const handlePasswordChange = (val: string) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        formIsReady: data.isValidUser && data.username.length > 0,
        isValidPassword: true,
        password: val
      });
    } else {
      setData({
        ...data,
        formIsReady: false,
        isValidPassword: false,
        password: val
      });
    }
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
    });
  };

  const handleValidUser = useCallback(
    (val: string) => {
      if (val.trim().length >= 4) {
        setData({
          ...data,
          isValidUser: true
        });
      } else {
        setData({
          ...data,
          isValidUser: false
        });
      }
    },
    [data]
  );
  const handleValidUserEvent = useCallback((e) => handleValidUser(e.nativeEvent.text), [
    handleValidUser
  ]);
  return (
    <View style={styles.container}>
      <Text
        style={[
          styles.text_footer,
          {
            color: Colors.text
          }
        ]}
      >
        {'Email'}
      </Text>
      <View style={styles.action}>
        <Icon name="person-circle-outline" color={Colors.text} size={20} />
        <TextInput
          keyboardType="email-address"
          placeholder="email@dominio.com"
          placeholderTextColor="#666666"
          style={[
            styles.textInput,
            {
              color: Colors.text
            }
          ]}
          autoCapitalize="none"
          onChangeText={textInputChange}
          onEndEditing={handleValidUserEvent}
        />
        {data.check_textInputChange ? (
          <Animatable.View animation="bounceIn">
            <Icon name="checkmark-outline" color="green" size={20} />
          </Animatable.View>
        ) : null}
      </View>
      {data.isValidUser ? null : (
        <Animatable.View animation="fadeInLeft" duration={500}>
          <Text style={styles.errorMsg}>{'El email es inválido'}</Text>
        </Animatable.View>
      )}

      <Text
        style={[
          styles.text_footer,
          {
            color: Colors.text,
            marginTop: 35
          }
        ]}
      >
        {'Contraseña'}
      </Text>
      <View style={styles.action}>
        <Icon name="lock-closed-outline" color={Colors.text} size={20} />
        <TextInput
          placeholder="Your Password"
          placeholderTextColor="#666666"
          secureTextEntry={data.secureTextEntry ? true : false}
          style={[
            styles.textInput,
            {
              color: Colors.text
            }
          ]}
          autoCapitalize="none"
          onChangeText={(val) => handlePasswordChange(val)}
        />
        <TouchableOpacity onPress={updateSecureTextEntry}>
          {data.secureTextEntry ? (
            <Icon name="eye-off" color="grey" size={20} />
          ) : (
            <Icon name="eye" color="grey" size={20} />
          )}
        </TouchableOpacity>
      </View>
      {data.isValidPassword ? null : (
        <Animatable.View animation="fadeInLeft" duration={500}>
          <Text style={styles.errorMsg}>{'La contraseña contiene al menos 4 carácteres'}</Text>
        </Animatable.View>
      )}

      <TouchableOpacity>
        <BodyMedium style={{ color: Colors.primaryColor, marginTop: 15 }}>
          {'¿Olvidaste tu contraseña?'}
        </BodyMedium>
      </TouchableOpacity>

      <Button style={[styles.button]} disabled={!data.formIsReady} onPress={submitHandle}>
        {'Ingresar'}
      </Button>
    </View>
  );
};

export default LoginForm;
