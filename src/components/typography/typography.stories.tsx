import { Picker } from '@react-native-picker/picker';
import React, { useCallback, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

import { COLORS, ColorTypeProp } from 'benjaminmoore-core/lib/constants/colors';

import BodyLarge from './body-large';
import BodyMedium from './body-medium';
import BodySmall from './body-small';
import H1 from './h1';
import H2 from './h2';
import H3 from './h3';
import H4 from './h4';
import { TextProps } from './text';
import Box from '~/components/grid/box';
import Grid from '~/components/grid/grid';

const styles = StyleSheet.create({
  textInput: {
    borderColor: '#272727',
    borderWidth: 1,
    marginBottom: 10,
    marginTop: 5
  }
});

type ComponentTypes = 'BodyLarge' | 'BodyMedium' | 'BodySmall' | 'H1' | 'H2' | 'H3' | 'H4';

const COMPONENTS = { BodyLarge, BodyMedium, BodySmall, H1, H2, H3, H4 };

const FONT_WEIGHTS = ['thin', 'light', 'regular', 'medium', 'bold', 'black'];

const FONT_STYLES = ['normal', 'italic'];

// FIXME: migrate to actually use storybook
const Typography = (): React.ReactElement => {
  const [color, setColor] = useState('black' as ColorTypeProp);
  const [text, setText] = useState('Sample text');
  const [component, setComponent] = useState('H1');
  const [fontStyle, setFontStyle] = useState('normal');
  const [fontWeight, setFontWeight] = useState('thin');
  const [numberOfLines, setNumberOfLines] = useState(undefined);

  const Component = COMPONENTS[component as ComponentTypes];

  const onColorChange = useCallback((value) => setColor(value), []);
  const onComponentChange = useCallback((value) => setComponent(value), []);
  const onFontStyleChange = useCallback((value) => setFontStyle(value), []);
  const onFontWeightChange = useCallback((value) => setFontWeight(value), []);
  const onNumberOfLinesChange = useCallback((value) => setNumberOfLines(value), []);

  return (
    <Grid>
      <Box>
        <BodyMedium>{'Options:'}</BodyMedium>
        <BodySmall>{'Text:'}</BodySmall>
        <TextInput value={text} onChangeText={setText} style={styles.textInput} />
        <BodySmall>{'Component:'}</BodySmall>
        <Picker selectedValue={component} onValueChange={onComponentChange}>
          {Object.keys(COMPONENTS).map((componentName) => (
            <Picker.Item key={componentName} label={componentName} value={componentName} />
          ))}
        </Picker>
        <BodySmall>{'Color:'}</BodySmall>
        <Picker selectedValue={color} onValueChange={onColorChange}>
          {Object.keys(COLORS).map((mappedColor) => (
            <Picker.Item key={mappedColor} label={mappedColor} value={mappedColor} />
          ))}
        </Picker>
        <BodySmall>{'Style:'}</BodySmall>
        <Picker selectedValue={fontStyle} onValueChange={onFontStyleChange}>
          {FONT_STYLES.map((mappedFontStyle) => (
            <Picker.Item key={mappedFontStyle} label={mappedFontStyle} value={mappedFontStyle} />
          ))}
        </Picker>
        <BodySmall>{'Weight:'}</BodySmall>
        <Picker selectedValue={fontWeight} onValueChange={onFontWeightChange}>
          {FONT_WEIGHTS.map((mappedFontWeight) => (
            <Picker.Item key={mappedFontWeight} label={mappedFontWeight} value={mappedFontWeight} />
          ))}
        </Picker>
        <BodySmall>{'Number of lines:'}</BodySmall>
        <TextInput
          value={numberOfLines}
          onChange={onNumberOfLinesChange}
          keyboardType="decimal-pad"
          style={styles.textInput}
        />
        <BodyMedium>{'Result:'}</BodyMedium>
        <Component
          color={color}
          fontStyle={fontStyle as TextProps['fontStyle']}
          fontWeight={fontWeight as TextProps['fontWeight']}
          numberOfLines={numberOfLines}
        >
          {text}
        </Component>
      </Box>
    </Grid>
  );
};

export default Typography;
