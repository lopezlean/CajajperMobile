import React from 'react';
import { StyleSheet, TextStyle } from 'react-native';

import { H1 as H1_STYLE } from '~/constants/Typography';

import Text, { TextProps } from './text';

const styles = StyleSheet.create({
  container: {
    ...H1_STYLE
  }
});

const H1 = ({ children, style, ...props }: TextProps): React.ReactElement => (
  <Text {...props} style={[style as TextStyle, styles.container]}>
    {children}
  </Text>
);

export default H1;
