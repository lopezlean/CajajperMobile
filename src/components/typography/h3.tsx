import React from 'react';
import { StyleSheet, TextStyle } from 'react-native';

import { H3 as H3_STYLE } from '~/constants/Typography';

import Text, { TextProps } from './text';

const styles = StyleSheet.create({
  container: {
    ...H3_STYLE
  }
});

const H3 = ({ children, style, ...props }: TextProps): React.ReactElement => (
  <Text {...props} style={[style as TextStyle, styles.container]}>
    {children}
  </Text>
);

export default H3;
