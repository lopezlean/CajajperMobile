import React from 'react';
import { StyleSheet, TextStyle } from 'react-native';

import { H2 as H2_STYLE } from '~/constants/Typography';

import Text, { TextProps } from './text';

const styles = StyleSheet.create({
  container: {
    ...H2_STYLE
  }
});

const H2 = ({ children, style, ...props }: TextProps): React.ReactElement => (
  <Text {...props} style={[style as TextStyle, styles.container]}>
    {children}
  </Text>
);

export default H2;
