import React from 'react';
import { Text as ReactNativeText, StyleSheet, TextStyle } from 'react-native';

import Colors from '~/constants/Colors';
import useColorScheme from '~/hooks/useColorScheme';

/* eslint-disable sort-keys */
const mapping = {
  thin: '100',
  light: '300',
  regular: 'normal',
  medium: '500',
  bold: 'bold',
  black: '900'
};

/* eslint-enable sort-keys */

type StyleProps = Omit<TextProps, 'children' | 'style'> & {
  theme: 'light' | 'dark';
};

const getStyles = ({ theme, fontStyle, fontWeight }: StyleProps) =>
  StyleSheet.create({
    container: {
      color: Colors[theme].tint,
      fontFamily: 'Roboto',
      fontStyle: fontStyle || 'normal',
      fontWeight: mapping[fontWeight || 'regular'] as TextStyle['fontWeight']
    }
  });

const Text = ({
  children,
  fontStyle,
  fontWeight,
  numberOfLines,
  style
}: TextProps): React.ReactElement => {
  const theme = useColorScheme();

  return (
    <ReactNativeText
      numberOfLines={numberOfLines}
      style={[getStyles({ fontStyle, fontWeight, theme }).container, style]}
    >
      {children}
    </ReactNativeText>
  );
};

export type TextProps = {
  children?: React.ReactNode;
  fontStyle?: 'normal' | 'italic';
  fontWeight?: 'regular' | 'medium' | 'bold' | 'thin' | 'black' | 'light';
  style?: TextStyle | TextStyle[];
  numberOfLines?: number;
};

export default Text;
