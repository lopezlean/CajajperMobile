import React from 'react';
import { StyleSheet, TextStyle } from 'react-native';

import { BODY_SMALL } from '~/constants/Typography';

import Text, { TextProps } from './text';

const styles = StyleSheet.create({
  container: {
    ...BODY_SMALL
  }
});

const BodySmall = ({ children, style, ...props }: TextProps): React.ReactElement => (
  <Text {...props} style={[style as TextStyle, styles.container]}>
    {children}
  </Text>
);

export default BodySmall;
