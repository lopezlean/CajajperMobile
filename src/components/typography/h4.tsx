import React from 'react';
import { StyleSheet, TextStyle } from 'react-native';

import { H4 as H4_STYLE } from '~/constants/Typography';

import Text, { TextProps } from './text';

const styles = StyleSheet.create({
  container: {
    ...H4_STYLE
  }
});

const H4 = ({ children, style, ...props }: TextProps): React.ReactElement => (
  <Text {...props} style={[style as TextStyle, styles.container]}>
    {children}
  </Text>
);

export default H4;
