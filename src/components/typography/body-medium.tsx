import React from 'react';
import { StyleSheet, TextStyle } from 'react-native';

import { BODY_MEDIUM } from '~/constants/Typography';

import Text, { TextProps } from './text';

const styles = StyleSheet.create({
  container: {
    ...BODY_MEDIUM
  }
});

const BodyMedium = ({ children, style, ...props }: TextProps): React.ReactElement => (
  <Text {...props} style={[style as TextStyle, styles.container]}>
    {children}
  </Text>
);

export default BodyMedium;
