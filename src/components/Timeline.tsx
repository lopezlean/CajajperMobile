import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Animated, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Colors from '~/constants/Colors';

const isAndroid = Platform.OS === 'android';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%'
  },
  description: {
    color: '#999',
    fontSize: 11,
    lineHeight: 18,
    paddingBottom: 10,
    textAlign: 'left'
  },
  eventContainer: {
    alignItems: 'flex-start',
    backgroundColor: '#FFF',
    borderRadius: 15,
    borderTopLeftRadius: 0,
    flexBasis: '55%',
    marginBottom: 10,
    padding: 16,
    shadowColor: '#ccc',
    shadowOffset: {
      height: 0,
      width: -8
    },
    shadowOpacity: 0.2
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.light.loadingBackgroundColor,
    borderColor: Colors.light.loadingBackgroundColor,
    borderRadius: 15,
    borderWidth: 3,
    color: Colors.text,
    fontSize: 9,
    height: 30,
    overflow: 'hidden',
    paddingTop: isAndroid ? 6 : 2.5,
    textAlign: 'center',
    width: 30,
    fontSize: 15
  },
  iconContainer: {
    alignItems: 'center',
    alignSelf: 'stretch',
    flexBasis: '6%',
    marginHorizontal: '5%'
  },
  row: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginVertical: 5
  },
  time: {
    color: '#aaa',
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'center'
  },
  timeContainer: {
    flexBasis: '25%'
  },
  title: {
    color: '#666',
    fontSize: 12,
    fontWeight: 'bold',
    lineHeight: 20,
    marginBottom: 5,
    textAlign: 'left'
  },
  verticalLine: {
    backgroundColor: '#ededed',
    flex: 1,
    width: 1
  }
});

const EventTime = ({ time: { content, style: timeStyle } = {}, style }) => {
  return (
    <View style={[styles.timeContainer, style]}>
      <Text style={[styles.time, timeStyle]}>{content}</Text>
    </View>
  );
};

const EventIcon = ({ icon: OriginalIcon = {}, iconStyle, lineStyle }) => {
  // Determines whether we are trying to render a custom icon component, or use the default
  const iconIsComponent = typeof OriginalIcon === 'function';
  const iconToBeRendered = iconIsComponent ? (
    <OriginalIcon styles={styles.icon} />
  ) : (
    <Icon
      name={OriginalIcon.content}
      style={[styles.icon, OriginalIcon.style && OriginalIcon.style ? OriginalIcon.style : null]}
    />
  );

  return (
    <View style={[styles.iconContainer, iconStyle]}>
      {iconToBeRendered}
      <View style={[styles.verticalLine, lineStyle]} />
    </View>
  );
};

/*
Event component, is the component in which you can render whatever the event is about,
e.g. Title, description, or even render a custom template by sending a render-prop with whatsoever
content you need.
*/
const Event = ({ children, style }) => {
  return <View style={[styles.eventContainer, style]}>{children}</View>;
};

/*
Row component, is the component that combines all the sub-components (EventIcon, Event, EventTime) and
gets each 'event' as an argument of type object
*/
const Row = ({
  event = {},
  eventStyle,
  timeContainerStyle,
  iconContainerStyle,
  lineStyle,
  contentContainerStyle
}) => {
  const {
    title: OriginalTitle = {},
    description: OriginalDescription = {},
    time,
    icon,
    pressAction
  } = event;

  // Determines whether or not the Row is clickable, and acts accordingly
  const RowComp = pressAction ? TouchableOpacity : View;

  // Determines whether the title is just a text and its style, or a render-prop function, and acts accrodingly
  const titleIsComponent = OriginalTitle && typeof OriginalTitle === 'function';
  const title = titleIsComponent ? (
    <OriginalTitle styles={styles.title} />
  ) : (
    typeof OriginalTitle === 'object' &&
    OriginalTitle && (
      <Text style={[styles.title, OriginalTitle.style]}>{OriginalTitle.content}</Text>
    )
  );

  // Determines whether the description is just a text and its style, or a render-prop function, and acts accrodingly
  const descriptionIsComponent = OriginalDescription && typeof OriginalDescription === 'function';
  const description = descriptionIsComponent ? (
    <OriginalDescription styles={styles.description} />
  ) : (
    typeof OriginalDescription === 'object' &&
    OriginalDescription && (
      <Text style={[styles.description, OriginalDescription.style]}>
        {OriginalDescription.content}
      </Text>
    )
  );

  return (
    <RowComp style={[styles.row, eventStyle]} onPress={pressAction}>
      <EventTime time={time} style={timeContainerStyle} />
      <EventIcon icon={icon} iconStyle={iconContainerStyle} lineStyle={lineStyle} />
      <Event style={contentContainerStyle}>
        {title}
        {description}
      </Event>
    </RowComp>
  );
};

const Timeline = React.forwardRef(
  (
    {
      data = [], // The actual event's array, array of objects, each object represents a single event
      eventStyle = {}, // Each event's whole row's style
      timeContainerStyle = {}, // The style object of the container that holds the time
      iconContainerStyle = {}, // The style object of the container that holds the icon
      lineStyle = {}, // The vertical line's style object
      onEndReachedThreshold,
      onEndReached,
      TimelineFooter,
      TimelineHeader,
      ...rest
    },
    ref
  ) => {
    const events = (
      <Animated.FlatList
        ref={ref}
        data={data}
        renderItem={({ item }) => (
          <Row
            event={item}
            eventStyle={eventStyle}
            timeContainerStyle={timeContainerStyle}
            iconContainerStyle={iconContainerStyle}
            lineStyle={lineStyle}
          />
        )}
        keyExtractor={(_, ndx) => ndx.toString()}
        onEndReached={onEndReached}
        onEndReachedThreshold={onEndReachedThreshold || 0}
        ListFooterComponent={TimelineFooter}
        ListHeaderComponent={TimelineHeader}
        {...rest}
      />
    );

    return <View style={styles.container}>{events}</View>;
  }
);

export default Animated.createAnimatedComponent(Timeline);
