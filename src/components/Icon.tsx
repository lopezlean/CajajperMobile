import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Colors from '~/constants/Colors';
import useColorScheme from '~/hooks/useColorScheme';

export type IconSizeType = 16 | 24 | 32 | 48;

export type IconProps = {
  color?: string;
  icon: string;
  size?: IconSizeType;
};

const Icon = ({ icon, size = 24, color }: IconProps): React.ReactElement => {
  const theme = useColorScheme();

  const colorToUse = color ? color : theme === 'light' ? Colors.light.tint : Colors.dark.tint;
  return <Ionicons name={icon} size={size} color={colorToUse} />;
};

export default Icon;
