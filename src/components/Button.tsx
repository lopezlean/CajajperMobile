import React, { useCallback, useMemo, useState } from 'react';
import {
  Text,
  Pressable,
  View,
  StyleSheet,
  GestureResponderEvent,
  StyleProp,
  ViewStyle
} from 'react-native';

import Icon, { IconSizeType } from '~/components/Icon';
import Colors from '~/constants/Colors';
import { BUTTON, SMALL_BUTTON } from '~/constants/Typography';
import useColorScheme from '~/hooks/useColorScheme';

interface PaddingInterface {
  paddingBottom: number;
  paddingLeft: number;
  paddingRight: number;
  paddingTop: number;
}

const paddingAssign = (a: number, b?: number, c?: number, d?: number): PaddingInterface => ({
  paddingBottom: c ? c : a,
  paddingLeft: d ? d : b ? b : a,
  paddingRight: b ? b : a,
  paddingTop: a
});
type ButtonType = 'primary' | 'secondary' | 'outline' | 'contrast';
interface StylePropsInterface {
  readonly colorScheme: 'light' | 'dark';
  readonly backgroundColor?: string;
  readonly color?: string;
  readonly disabled?: boolean;
  readonly iconMarginRight?: number;
  readonly iconOnly?: boolean;
  readonly iconOnlyRounded?: boolean;
  readonly padding?: PaddingInterface;
  readonly pressed?: boolean;
  readonly icon?: string;
  readonly type: ButtonType;
  readonly small?: boolean;
}

interface ThemeProps {
  backgroundColor: string;
  color: string;
  borderColor: string;
}
interface ThemeInterface {
  outline: ThemeProps;
  primary: ThemeProps;
  secondary: ThemeProps;
  contrast: ThemeProps;
}
const theme = (colorScheme: 'light' | 'dark') => ({
  contrast: {
    backgroundColor: Colors[colorScheme].constrastButton.backgroundColor,
    borderColor: 'transparent',
    color: Colors[colorScheme].constrastButton.color
  },
  outline: {
    backgroundColor: '#fff',
    borderColor: Colors[colorScheme].button.backgroundColor,
    color: Colors[colorScheme].button.backgroundColor
  },
  primary: {
    backgroundColor: Colors[colorScheme].button.backgroundColor,
    borderColor: 'transparent',
    color: Colors[colorScheme].button.color
  },
  secondary: {
    backgroundColor: Colors[colorScheme].secondaryButton.backgroundColor,
    borderColor: 'transparent',
    color: Colors[colorScheme].secondaryButton.color
  }
});

const getStyles = ({
  colorScheme,
  backgroundColor,
  color,
  small,
  disabled,
  icon,
  iconMarginRight,
  iconOnly,
  iconOnlyRounded,
  padding,
  type,
  pressed
}: StylePropsInterface) =>
  StyleSheet.create({
    container: {
      alignItems: 'center',
      alignSelf: 'flex-start',
      backgroundColor: backgroundColor || theme(colorScheme)[type].backgroundColor,
      borderColor: theme(colorScheme)[type].borderColor,
      borderRadius: icon && iconOnly && iconOnlyRounded ? 50 : 10,
      borderWidth: type === 'outline' ? 1 : 0,
      color: color || theme(colorScheme)[type].color,
      opacity: disabled ? 0.6 : 1,
      transform: pressed && !disabled ? [{ scale: 0.98 }] : [],
      ...padding
    },
    content: {
      alignContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    icon: {
      marginRight: iconMarginRight
    },
    text: {
      color,
      fontFamily: 'Roboto',
      fontSize: small ? SMALL_BUTTON.fontSize : BUTTON.fontSize,
      fontStyle: 'normal',
      fontWeight: 'bold',
      lineHeight: small ? SMALL_BUTTON.lineHeight : BUTTON.lineHeight
    }
  });

export type ButtonProps = {
  accessibilityLabel?: string;
  backgroundColor?: string;
  children?: React.ReactNode;
  type?: ButtonType;
  color?: string;
  style?: StyleProp<ViewStyle> & { color?: string };
  contentStyles?: StyleProp<ViewStyle>;
  disabled?: boolean;
  icon?: string;
  iconOnly?: boolean;
  iconOnlyRounded?: boolean;
  iconSize?: IconSizeType;
  iconStyles?: StyleProp<ViewStyle>;
  onPress?: (event: GestureResponderEvent) => void;
  small?: boolean;
};

const defaultIconMarginRight = 0;

const SMALL_PADDING = 5;
const DEFAULT_PADDING = 15;

const Button = ({
  accessibilityLabel,
  backgroundColor,
  children,
  type = 'primary',
  color,
  contentStyles,
  style,
  iconStyles,
  iconSize,
  disabled,
  icon,
  iconOnly,
  iconOnlyRounded,
  onPress,
  small
}: ButtonProps): React.ReactElement => {
  const colorScheme = useColorScheme();
  const [pressed, setPressed] = useState(false);
  const textColor = color || theme(colorScheme)[type].color;

  const { iconMarginRight, padding } = useMemo(() => {
    const values = {
      iconMarginRight: defaultIconMarginRight,
      // Default (or small) without icon
      padding: paddingAssign(small ? SMALL_PADDING : DEFAULT_PADDING)
    };

    if (icon && !iconOnly) {
      values.iconMarginRight = small ? SMALL_PADDING : DEFAULT_PADDING;
    }

    if (icon && iconOnly) {
      values.padding = paddingAssign(small ? SMALL_PADDING : DEFAULT_PADDING);
    } else if (small && icon) {
      // small with icon
      values.padding = paddingAssign(SMALL_PADDING, DEFAULT_PADDING);
    } else if (icon) {
      // Default with icon
      values.padding = paddingAssign(DEFAULT_PADDING);
    }

    return values;
  }, [icon, iconOnly, small]);

  const computedStyles = useMemo(
    () =>
      getStyles({
        colorScheme,
        backgroundColor,
        color: textColor,
        disabled,
        icon,
        iconMarginRight,
        iconOnly,
        iconOnlyRounded,
        padding,
        pressed,
        small,
        type
      }),
    [
      colorScheme,
      backgroundColor,
      disabled,
      icon,
      iconMarginRight,
      iconOnly,
      iconOnlyRounded,
      padding,
      pressed,
      type,
      small,
      textColor
    ]
  );

  const onPressInCallback = useCallback(() => {
    setPressed(true);
  }, []);
  const onPressOutCallback = useCallback(() => {
    setPressed(false);
  }, []);

  return (
    <Pressable
      accessibilityLabel={accessibilityLabel}
      disabled={disabled}
      onPress={onPress}
      style={[computedStyles.container, style]}
      onPressIn={onPressInCallback}
      onPressOut={onPressOutCallback}
    >
      <View style={[computedStyles.content, contentStyles]}>
        {icon ? (
          <View style={[computedStyles.icon, iconStyles]}>
            <Icon color={textColor} icon={icon} size={iconSize || small ? 16 : 24} />
          </View>
        ) : null}
        {iconOnly ? null : (
          <Text style={computedStyles.text} numberOfLines={1}>
            {children}
          </Text>
        )}
      </View>
    </Pressable>
  );
};

export default Button;
