import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { ActionSheetIOS } from 'react-native';

import Button from './Button';

interface PickerInterface {
  children: string;
  value: unknown;
  items: {
    value: unknown;
    label: string;
  }[];
  onValueChange: (value: unknown) => null;
}

const Picker = ({ items, children, onValueChange, value }: PickerInterface): React.ReactElement => {
  const [currentItem, setCurrentItem] = useState('');
  const parsedItemsOptions = useMemo(() => {
    return items.map((item) => {
      return item.label;
    });
  }, [items]);

  const parsedItemsMap = useMemo(() => {
    return items.map((item) => {
      return item.value;
    });
  }, [items]);

  const onPress = useCallback(() => {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: parsedItemsOptions
      },
      (buttonIndex) => {
        onValueChange(parsedItemsMap[buttonIndex]);
        setCurrentItem(parsedItemsOptions[buttonIndex]);
      }
    );
  }, [onValueChange, parsedItemsMap, parsedItemsOptions]);

  useEffect(() => {
    if (value) {
      const index = parsedItemsMap.findIndex((element) => element === value);
      if (index !== undefined) {
        setCurrentItem(parsedItemsOptions[index]);
      }
    }
  }, [parsedItemsMap, parsedItemsOptions, value]);

  return (
    <Button type="secondary" small={true} onPress={onPress}>
      {currentItem || children}
    </Button>
  );
};

export default Picker;
