import React from 'react';
import { View, Pressable, StyleSheet } from 'react-native';

import Button from '../Button';
import { Text } from '../Text';
import Icon from '~/components/Icon';
import Colors from '~/constants/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    marginTop: 10,
    textAlign: 'center'
  },
  button: {
    marginTop: 10,
    alignSelf: 'center'
  }
});
interface ErroMessageInterface {
  icon?: string;
  text: string;
  button?: { text: string; onPress: () => null; styles: unknown };
  children?: React.ReactElement | React.ReactNode | undefined;
}
const EmptyMessage = ({
  icon = 'bug-outline',
  text,
  children,
  button
}: ErroMessageInterface): React.ReactElement => {
  const renderButton = button ? (
    <Button onPress={button.onPress} style={styles.button}>
      {button.text}
    </Button>
  ) : null;
  return (
    <View style={styles.container}>
      <Icon icon={icon} size={50} />
      <Text style={styles.text}>{text}</Text>
      {renderButton}
      {children}
    </View>
  );
};

export default EmptyMessage;
