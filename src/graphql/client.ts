import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import { setContext } from '@apollo/client/link/context';
import * as SecureStore from 'expo-secure-store';

import Enviroment from '~/constants/Enviroment';

const httpLink = createHttpLink({
  uri: Enviroment.apiUrl
});

const authLink = setContext(async (_, { headers }) => {
  // get the authentication token from local storage if it exists
  try {
    const token = await SecureStore.getItemAsync('userToken');
    // return the headers to the context so httpLink can read them

    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    };
  } catch (e) {
    console.log(e, 'setContext')
  }
});

const errorLink = onError(({ graphQLErrors, networkError, operation, forward }) => {
  console.log('onError', graphQLErrors);
  if (graphQLErrors) {
    for (const err of graphQLErrors) {
      // handle errors differently based on its error code
      switch (err.extensions.code) {
        case 'UNAUTHENTICATED':
          // old token has expired throwing AuthenticationError,
          // one way to handle is to obtain a new token and
          // add it to the operation context
          const headers = operation.getContext().headers;

          //console.warn('TODO: UNAUTHENTICATED')

          // Now, pass the modified operation to the next link
          // in the chain. This effectively intercepts the old
          // failed request, and retries it with a new token
          return forward(operation);
        case 'FORBIDDEN':
          return forward(operation);
        // handle other errors
        case 'ANOTHER_ERROR_CODE':
        // ...
        case 'INTERNAL_SERVER_ERROR':
          console.log('INTERNAL SERVER ERROR');
          return forward(operation);
      }
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(errorLink).concat(httpLink),
  cache: new InMemoryCache()
});

export default client;
