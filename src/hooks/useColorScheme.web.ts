// useColorScheme from react-native does not support web currently. You can replace
// this with react-native-appearance if you would like theme support on web.
const useColorScheme = (): string => 'light';
export default useColorScheme;
