import React from 'react';

export default React.createContext({
  signIn: async (data) => {
    console.log({ data });
  },
  signOut: async () => {},
  signUp: async (data) => {}
});
