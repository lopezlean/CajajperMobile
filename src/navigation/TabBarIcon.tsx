import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
const TabBarIcon = (props: {
  name: React.ComponentProps<typeof Ionicons>['name'];
  color: string;
}): React.ReactElement => <Ionicons size={20} style={{}} {...props} />;

export default TabBarIcon;
