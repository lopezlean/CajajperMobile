import { BottomTabBarButtonProps } from '@react-navigation/bottom-tabs';
import React, { useMemo } from 'react';
import { Platform, Pressable, StyleSheet } from 'react-native';

import Colors from '~/constants/Colors';
import useColorScheme from '~/hooks/useColorScheme';

interface TabButtonProps extends BottomTabBarButtonProps {
  isActive: boolean;
}

const isAndroid = Platform.OS === 'android';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    alignSelf: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: isAndroid ? 2 : 0,
    marginTop: isAndroid ? 2 : 5,
    paddingVertical: 5,
    position: 'relative'
  }
});

const TabBarButton = ({ children, isActive, ...props }: TabButtonProps): React.ReactElement => {
  const theme = useColorScheme();
  const style = useMemo(
    () => ({
      backgroundColor: isActive ? Colors[theme].primaryColor : 'transparent',
      borderRadius: isActive ? 20 : 0
    }),
    [isActive, theme]
  );
  return (
    <Pressable {...props} style={[styles.container, style]}>
      {children}
    </Pressable>
  );
};

export default TabBarButton;
