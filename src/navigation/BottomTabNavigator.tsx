import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator, StackCardInterpolationProps } from '@react-navigation/stack';
import React, { useCallback } from 'react';
import { createSharedElementStackNavigator } from 'react-navigation-shared-element';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import DocumentsScreen from '../screens/DocumentsScreen';
import HomeScreen from '../screens/HomeScreen';
import { BottomTabParamList, HomeParamList, DocumentsParamList } from '../types';
import TabBarButton from './TabBarButton';
import TabBarIcon from './TabBarIcon';
import AppSearchHeader from '~/components/AppSearchHeader';
import PaycheckNavigationDrawer from '~/components/Paycheck/Drawer/PaycheckNavigationDrawer';
import DocumentViewScreen from '~/screens/DocumentViewScreen';
import NewsViewScreen from '~/screens/NewsViewScreen';
import PaycheckAssociateAccountScreen from '~/screens/Paycheck/PaycheckAssociateAccountScreen';
import PaycheckScreen from '~/screens/PaycheckScreen';
import QrCodeScreen from '~/screens/QrCodeScreen';
import SettingsScreen from '~/screens/SettingsScreen';

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const HomeStack = createSharedElementStackNavigator<HomeParamList>();
const NewsViewScreenOptions = (theme: 'light' | 'dark') => {
  return {
    cardStyleInterpolator: ({ current: { progress } }: StackCardInterpolationProps) => {
      return {
        cardStyle: {
          opacity: progress
        }
      };
    },
    headerBackTitleVisible: true,
    headerStyle: { backgroundColor: Colors[theme].primaryColor },
    headerTintColor: Colors[theme].tint,
    headerTitle: 'Ver novedad'
  };
};

const HomeNavigator = () => {
  const theme = useColorScheme();
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          headerStyle: { backgroundColor: Colors[theme].primaryColor },
          headerTintColor: Colors.dark.tint,
          headerTitle: 'Novedades'
        }}
      />
      <HomeStack.Screen
        name="NewsViewScreen"
        component={NewsViewScreen}
        options={NewsViewScreenOptions(theme)}
      />
    </HomeStack.Navigator>
  );
};

const DocumentsStack = createStackNavigator<DocumentsParamList>();

const DocumentsNavigator = () => {
  const theme = useColorScheme();
  const options = useCallback(
    ({ route }) => ({
      headerBackTitle: 'Atrás',
      headerStyle: {
        backgroundColor: Colors[theme].primaryColor,
        shadowColor: 'transparent',
        shadowOffset: { height: 0 },
        shadowRadius: 0
      },
      headerTintColor: '#fff',

      title: `${route.params.item.id}`
    }),
    []
  );
  return (
    <DocumentsStack.Navigator headerMode="screen">
      <DocumentsStack.Screen
        name="Documents"
        component={DocumentsScreen}
        options={{ header: AppSearchHeader }}
      />
      <DocumentsStack.Screen
        name="QrCode"
        component={QrCodeScreen}
        options={{ headerBackTitle: 'Atrás', title: 'Escanear QR' }}
      />
      <DocumentsStack.Screen
        name="DocumentsView"
        component={DocumentViewScreen}
        options={options}
      />
    </DocumentsStack.Navigator>
  );
};

const ReciboDigitalStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const PaycheckNavigatorDrawer = () => {
  const drawerRender = useCallback((props) => <PaycheckNavigationDrawer {...props} />, []);
  return (
    <Drawer.Navigator initialRouteName="Paycheck" drawerContent={drawerRender}>
      <Drawer.Screen
        name="Paycheck"
        component={PaycheckScreen}
        options={{ headerTitle: 'Recibo digital' }}
      />
    </Drawer.Navigator>
  );
};

const PaycheckNavigator = () => {
  const theme = useColorScheme();
  return (
    <ReciboDigitalStack.Navigator>
      <ReciboDigitalStack.Screen
        name="PaycheckDrawer"
        component={PaycheckNavigatorDrawer}
        options={{ headerTitle: 'Recibo digital' }}
      />

      <ReciboDigitalStack.Screen
        name="PaycheckAssociateAccount"
        component={PaycheckAssociateAccountScreen}
        options={{
          headerTitle: null,
          headerBackTitle: 'Atrás',
          headerStyle: { backgroundColor: Colors[theme].primaryColor, shadowOffset: { height: 0 } },
          headerTintColor: '#fff'
        }}
      />
    </ReciboDigitalStack.Navigator>
  );
};

const SettingsStack = createStackNavigator();

const SettingsNavigator = () => {
  const theme = useColorScheme();
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          headerStyle: { backgroundColor: Colors[theme].primaryColor },
          headerTintColor: Colors[theme].header.textColor,
          headerTitle: 'Configuraciones'
        }}
      />
    </SettingsStack.Navigator>
  );
};

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

const BottomTabNavigator = () => {
  const theme = useColorScheme();
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const renderOptions = useCallback((props: any, title: string, icon: string) => {
    const isActive = props.navigation.isFocused();

    return {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      tabBarButton: (tabProps: any) => <TabBarButton {...tabProps} isActive={isActive} />,
      tabBarIcon: ({ color }: { color: string }) => <TabBarIcon name={icon} color={color} />,
      tabBarLabel: title
    };
  }, []);
  const homeOptions = useCallback(
    (props) => renderOptions(props, 'Noticias', 'newspaper-outline'),
    [renderOptions]
  );

  const DocumentsOptions = useCallback(
    (props) => renderOptions(props, 'Expedientes', 'file-tray-stacked-outline'),
    [renderOptions]
  );
  const reciboOptions = useCallback((props) => renderOptions(props, 'Recibo', 'wallet-outline'), [
    renderOptions
  ]);
  const settingsOptions = useCallback(
    (props) => renderOptions(props, 'Configuraciones', 'settings-outline'),
    [renderOptions]
  );
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#fff',
        iconStyle: {
          position: 'relative'
        },
        inactiveTintColor: Colors[theme].inactiveTintColor,

        showLabel: true,
        style: {
          borderTopWidth: 0,
          position: 'relative',
          paddingHorizontal: 10
        },
        tabStyle: {
          position: 'relative'
        }
      }}
    >
      <BottomTab.Screen name="Home" component={HomeNavigator} options={homeOptions} />
      <BottomTab.Screen
        name="DocumentsStack"
        component={DocumentsNavigator}
        options={DocumentsOptions}
      />
      <BottomTab.Screen
        name="PaycheckStack"
        component={PaycheckNavigator}
        options={reciboOptions}
      />
      <BottomTab.Screen
        name="Configuraciones"
        component={SettingsNavigator}
        options={settingsOptions}
      />
    </BottomTab.Navigator>
  );
};

export default BottomTabNavigator;
