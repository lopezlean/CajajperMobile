export default {
  config: {
    screens: {
      NotFound: '*',
      Root: {
        screens: {
          Documents: {
            screens: {
              DocumentsViewScreen: 'documents',
              path: 'expedientes/view/:id'
            }
          },
          Home: {
            screens: {
              NewsScreen: 'News'
            }
          },
          Recibos: {
            screens: {
              PaycheckDrawer: {
                screens: {
                  PaycheckScreen: {
                    path: 'recibodigital'
                  },
                  PaycheckViewScreen: {
                    path: 'recibodigital/view'
                  }
                }
              }
            }
          },
          initialRouteName: 'NewsScreen'
        }
      }
    }
  },
  prefixes: ['/', 'https://expedientes.cajajper.gov.ar'],
};
