import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
// Stack

// Screens
import LoginScreen from '../screens/Auth/LoginScreen';
import RegisterScreen from '../screens/Auth/RegisterScreen';
import LogoutScreen from '../screens/Auth/LogoutScreen';
//import ForgotPasswordScreen from '../screens/Auth/ForgotPasswordScreen';

const Stack = createStackNavigator();

const AuthNavigator = (): React.ReactElement => {
  return (
    <Stack.Navigator initialRouteName="Login" headerMode="none">
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Register" component={RegisterScreen} />
      {/*<Stack.Screen name="Forgot" component={ForgotPasswordScreen} />*/}
      <Stack.Screen name="Logout" component={LogoutScreen} />
    </Stack.Navigator>
  );
};

export default AuthNavigator;
