export type TypographyProps = {
  fontSize: number;
  lineHeight: number;
};

export const NAVIGATION: TypographyProps = {
  fontSize: 16,
  lineHeight: 24
};

export const BUTTON: TypographyProps = {
  fontSize: 18,
  lineHeight: 24
};
export const SMALL_BUTTON: TypographyProps = {
  fontSize: 12,
  lineHeight: 16
};

export const BODY_SMALL: TypographyProps = {
  fontSize: 12,
  lineHeight: 16
};

export const BODY_MEDIUM: TypographyProps = {
  fontSize: 14,
  lineHeight: 18
};

export const BODY_LARGE: TypographyProps = {
  fontSize: 18,
  lineHeight: 23
};

export const H4: TypographyProps = {
  fontSize: 24,
  lineHeight: 28
};

export const H3: TypographyProps = {
  fontSize: 28,
  lineHeight: 32
};

export const H2: TypographyProps = {
  fontSize: 30,
  lineHeight: 34
};

export const H1: TypographyProps = {
  fontSize: 34,
  lineHeight: 40
};
