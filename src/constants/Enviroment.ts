import { Platform } from 'react-native';
import {
  DEV,
  API_URL,
  API_URL_ANDROID,
  API_URL_IOS,
  STATIC_FILES_URL,
  STATIC_FILES_URL_IOS,
  STATIC_FILES_URL_ANDROID
} from 'react-native-dotenv';

const isAndroid = Platform.OS === 'android';

console.log({
  DEV,
  API_URL,
  API_URL_ANDROID,
  API_URL_IOS,
  STATIC_FILES_URL,
  STATIC_FILES_URL_IOS,
  STATIC_FILES_URL_ANDROID
});

export default {
  androidClientId: '190225167822-hvfad06m4etnniv5n24s2mebab3o3o8d.apps.googleusercontent.com',
  androidStandaloneAppClientId:
    '190225167822-mr7k6a28m4u8va61l8q7i3l56bbju50j.apps.googleusercontent.com',
  apiUrl: DEV ? (isAndroid ? API_URL_ANDROID : 'http://192.168.0.81:4000/graphql') : API_URL,
  iosClientId: '190225167822-57soj7t03jk5tvpp37b07gtar5itr4jd.apps.googleusercontent.com',
  iosStandaloneAppClientId:
    '190225167822-3he321e8jqcbfbdprv2u81cbu9jmfkvj.apps.googleusercontent.com',
  staticFilesUrl: DEV
    ? isAndroid
      ? STATIC_FILES_URL_ANDROID
      : STATIC_FILES_URL_IOS
    : STATIC_FILES_URL
};
