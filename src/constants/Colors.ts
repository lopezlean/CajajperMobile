/* eslint-disable sort-keys */

import Color from 'color';

const primaryColor = '#00b1f5';
//const primaryColor = '#00253D';
const contrastColor = '#72a44d';
const accentColor = '#ff4f20';
const secondaryColor = '#72a44d';
const tintColorLight = '#080808';
const tintColorDark = '#fff';
const inactiveTintColor = 'black';

const buttonContrastColor = contrastColor;
const buttonPrimaryColor = primaryColor;
const buttonSecondaryColor = '#d6d6d6';

const primaryGradientColor = [primaryColor, Color(primaryColor).lighten(1).rgb().toString()];

const loadingBackgroundColor = '#efefef';

const text = '#000';

export default {
  light: {
    inactiveTintColor,
    primaryColor,
    text,
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    accentColor,
    loadingBackgroundColor,
    secondaryColor,
    primaryGradientColor,
    header: {
      textColor: '#fff'
    },
    button: {
      backgroundColor: buttonPrimaryColor,
      color: tintColorDark
    },
    secondaryButton: {
      backgroundColor: buttonSecondaryColor,
      color: tintColorLight
    },
    constrastButton: {
      backgroundColor: buttonContrastColor,
      color: tintColorDark
    },
    accentButton: {
      backgroundColor: accentColor,
      color: tintColorDark
    }
  },
  dark: {
    inactiveTintColor: '#a8a8a8',
    primaryColor: '#121212',
    text: '#fff',
    background: '#121212',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    accentColor,
    loadingBackgroundColor,
    secondaryColor,
    primaryGradientColor,
    header: {
      textColor: '#fff'
    },
    button: {
      backgroundColor: Color(buttonPrimaryColor).darken(0.5).toString(),
      color: tintColorDark
    },
    secondaryButton: {
      backgroundColor: Color(buttonSecondaryColor).darken(0.5).toString(),
      color: '#fff'
    },
    constrastButton: {
      backgroundColor: Color(buttonContrastColor).darken(0.5).toString(),
      color: tintColorDark
    },
    accentButton: {
      backgroundColor: Color(accentColor).darken(0.5).toString(),
      color: tintColorDark
    }
  }
};
