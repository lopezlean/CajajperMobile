import { DefaultTheme } from 'react-native-paper';
import { Theme } from 'react-native-paper/src/types';

import Colors from './Colors';

const lightTheme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.light.primaryColor,
    text: '#000'
  }

};

const darkTheme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.dark.primaryColor,
    text: '#d3d3d3'
  }
};

export { darkTheme, lightTheme };
