import * as React from 'react';

import ReciboDigitalAssociateAccount from '~/components/Paycheck/PaycheckAssociateAccount';
import Screen from '~/components/Screen';

const PaycheckAssociateAccountScreen = (): React.ReactElement => {
  return (
    <Screen padder={false}>
      <ReciboDigitalAssociateAccount />
    </Screen>
  );
};

export default PaycheckAssociateAccountScreen;
