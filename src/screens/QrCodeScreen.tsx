import { useLazyQuery } from '@apollo/client';
import gql from 'graphql-tag';
import React, { useCallback, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { RNCamera } from 'react-native-camera';
import QRCodeScanner from 'react-native-qrcode-scanner';

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});

const DOCUMENT_VIEW = gql`
  query documentView($query: Int!) {
    documentView(query: $query) {
      id
      applicant
      applicants {
        first_name
        last_name
      }
      office {
        name
      }
      last_movements {
        office {
          name
        }
        modified
      }
      benefit_type {
        name
      }
    }
  }
`;

const QrCodeScreen = (props): React.ReactElement => {
  const [documentView, { loading }] = useLazyQuery(DOCUMENT_VIEW, {
    fetchPolicy: 'network-only',
    onCompleted: (item) =>
      item && props.navigation.navigate('DocumentsView', { item: item.documentView })
  });
  const onSuccess = useCallback(
    (e) => {
      if (e.data) {
        const match = e.data.match(/expedientes\.cajajper\.gov\.ar\/expedientes\/view\/(\d+)/);
        if (match && match[1]) {
          const id = parseInt(match[1], 10);
          documentView({
            variables: {
              query: id
            }
          });
        }
      }
    },
    [documentView]
  );

  return (
    <QRCodeScanner
      reactivate={true}
      reactivateTimeout={3000}
      onRead={onSuccess}
      topContent={
        <Text style={styles.centerText}>
          {'Situal el codigo QR'}
          <Text style={styles.textBold}>{' en el centro de la pantalla.'}</Text>
        </Text>
      }
      bottomContent={
        <TouchableOpacity style={styles.buttonTouchable}>
          <Text style={styles.buttonText} onPress={props.navigation.goBack}>
            {loading ? 'Buscando...' : 'Cancelar'}
          </Text>
        </TouchableOpacity>
      }
    />
  );
};

export default QrCodeScreen;
