import { useRoute } from '@react-navigation/native';
import * as React from 'react';
import { StyleSheet, SafeAreaView, Animated, Image } from 'react-native';
import * as Animatable from 'react-native-animatable';
import WebView from 'react-native-webview';
import { useCollapsibleHeader } from 'react-navigation-collapsible';
import { SharedElement } from 'react-navigation-shared-element';

import { Text, View } from '../components/Themed';
import Button from '~/components/Button';
import Content from '~/components/Content';
import News from '~/components/News';
import { NewsItemInterface } from '~/components/News/NewsItemInterface';
import BodyMedium from '~/components/typography/body-medium';
import BodySmall from '~/components/typography/body-small';
import H2 from '~/components/typography/h2';
import Colors from '~/constants/Colors';
import navigation from '~/navigation';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  content: {
    flex: 1
  },
  scrollView: {
    paddingTop: 0
  },
  image: {
    width: '100%',
    height: 300
  },
  imageContainer: {
    flex: 1
  },
  title: {
    textAlign: 'right',
    backgroundColor: 'rgba(0,0,0,0.7)',
    color: '#fff',
    padding: 8,
    borderRadius: 10,
    overflow: 'hidden'
  },
  titleContainer: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    backgroundColor: 'transparent'
  }
});

const colllapsibleHeaderOptions = {
  config: {
    /* Optional */
    /* Optional, default: true */
    elevation: 4,
    /* Optional */
    useNativeDriver: true
  },
  navigationOptions: {
    headerStyle: {
      backgroundColor: Colors.primaryColor
    }
  }
};
const NewsViewScreen = (): React.ReactElement => {
  const {
    onScroll /* Event handler */,
    containerPaddingTop /* number */,
    scrollIndicatorInsetTop /* number */,

    translateY /* 0.0 ~ -headerHeight */
  } = useCollapsibleHeader(colllapsibleHeaderOptions);

  const [webviewHeight, setWebviewHight] = React.useState(10);
  const route = useRoute();
  const buttonRef = React.useRef();

  const { item } = route.params as { item: NewsItemInterface };
  console.log({ containerPaddingTop });
  return (
    <View style={styles.container}>
      <Animated.ScrollView
        style={styles.scrollView}
        onScroll={onScroll}
        contentContainerStyle={{ paddingTop: containerPaddingTop }}
        scrollIndicatorInsets={{ top: scrollIndicatorInsetTop }}
      >
        <View style={styles.content}>
          <View style={styles.imageContainer}>
            <SharedElement id={`item.${item.id}.image_url`}>
              <Image source={{ uri: item.image }} style={styles.image} resizeMode="cover" />
            </SharedElement>
            <View style={styles.titleContainer}>
              <SharedElement id={`item.${item.id}.title`}>
                <BodyMedium numberOfLines={3} style={styles.title}>
                  {item.title}
                </BodyMedium>
              </SharedElement>
            </View>
          </View>
          <View
            style={[
              styles.container,
              {
                backgroundColor: 'red',
                alignSelf: 'stretch',
                width: '100%',
                height: webviewHeight
              }
            ]}
          >
            <WebView
              originWhitelist={['*']}
              javaScriptEnabled={true}
              onMessage={(event) => {
                setWebviewHight(parseInt(event.nativeEvent.data));
              }}
              source={{
                html: `<html><head>
                <script>
                const onload = function () {
                  window.ReactNativeWebView.postMessage(document.documentElement.scrollHeight);

                };
              </script>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body onload="onload()">${item.body}</body></html>`
              }}
              domStorageEnabled={true}
              scrollEnabled={false}
              automaticallyAdjustContentInsets={false}
            />
          </View>
        </View>
      </Animated.ScrollView>
    </View>
  );
};

NewsViewScreen.sharedElements = (route) => {
  const { item } = route.params;
  return [
    {
      id: `item.${item.id}.image_url`,
      animation: 'move',
      resize: 'clip'
    },
    {
      id: `item.${item.id}.title`,
      animation: 'fade'
    },
    {
      id: `item.${item.id}.description`,
      animation: 'fade',
      resize: 'clip'
    },
    {
      id: `item.${item.id}.iconName`,
      animation: 'move',
      resize: 'clip'
    }
  ];
};

export default NewsViewScreen;
