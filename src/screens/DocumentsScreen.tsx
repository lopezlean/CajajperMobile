import * as React from 'react';
import { StyleSheet, ScrollView, SafeAreaView } from 'react-native';

import { View } from '../components/Themed';
import Search from '~/components/Documents/Search';

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

// TODO: Search or show favorites
const ExpedientesScreen = (): React.ReactElement => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        contentContainerStyle={styles.container}
        style={styles.container}
      >
        <View style={styles.container}>
          <Search />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ExpedientesScreen;
