import React, { useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import { Switch, Subheading, withTheme, DarkTheme, DefaultTheme } from 'react-native-paper';
import Button from '~/components/Button';
import AuthContext from '~/context/AuthContext';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    elevation: 2,
    padding: 16
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 16,
    paddingHorizontal: 16,
    width: '100%'
  }
});
const SettingsScreen = ({ theme }) => {
  const { signOut } = useContext(AuthContext);
  const { colors } = theme;

  return (
    <View style={[styles.container, { backgroundColor: colors.surface }]}>
      <View style={styles.row}>
        <Subheading style={{ color: colors.text }}>{'Notificaciones generales'}</Subheading>
        <Switch value={false} onValueChange={() => {}} />
      </View>
      <View style={styles.row}>
        <Subheading style={{ color: colors.text }}>{'Notificaciones de expedientes'}</Subheading>
        <Switch value={false} onValueChange={() => {}} />
      </View>
      <View style={styles.row}>
        <Subheading style={{ color: colors.text }}>{'Notificaciones de recibos'}</Subheading>
        <Switch value={false} onValueChange={() => {}} />
      </View>

      <View style={styles.row}>
        <Button onPress={signOut}>{'Cerrar sesión'}</Button>
      </View>
    </View>
  );
};
export default withTheme(SettingsScreen);
