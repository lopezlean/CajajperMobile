import * as React from 'react';
import { StyleSheet, ScrollView, SafeAreaView } from 'react-native';

import { View } from '../components/Themed';
import News from '~/components/News';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingTop: 5
  }
});

const TabOneScreen = () => {
  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.container}>
          <News />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default TabOneScreen;
