import * as React from 'react';

import PaycheckListItemInterface from '~/components/Paycheck/Interfaces/PaycheckListItemInterface';
import RecibosDigitalesList from '~/components/Paycheck/PaycheckHeader';
import PaycheckView from '~/components/Paycheck/PaycheckView';
import Screen from '~/components/Screen';

const ReciboDigitalScreen = ({ route }): React.ReactElement => {
  const item: PaycheckListItemInterface[] | PaycheckListItemInterface = route.params
    ? route.params.item
    : null;
  const renderItem = Array.isArray(item) ? item[0] : item;

  return (
    <Screen>
      <RecibosDigitalesList item={renderItem} />
      <PaycheckView item={item} />
    </Screen>
  );
};

export default ReciboDigitalScreen;
