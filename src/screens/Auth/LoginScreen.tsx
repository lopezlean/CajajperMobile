import { gql, useMutation } from '@apollo/client';
import React, { useCallback } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet,
  StatusBar,
  Alert
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import { useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

import Button from '~/components/Button';
import LoginForm, { SubmitData } from '~/components/LoginForm';
import BodyMedium from '~/components/typography/body-medium';
import Colors from '~/constants/Colors';
import AuthContext from '~/context/AuthContext';

const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      email
      token
    }
  }
`;

const styles = StyleSheet.create({
  action: {
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 5
  },
  actionError: {
    borderBottomColor: '#FF0000',
    borderBottomWidth: 1,
    flexDirection: 'row',
    marginTop: 10,
    paddingBottom: 5
  },
  button: {
    marginTop: 50,
    width: '100%'
  },
  container: {
    backgroundColor: Colors.primaryColor,
    flex: 1
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14
  },
  footer: {
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex: 3,
    paddingHorizontal: 20,
    paddingVertical: 30
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 50,
    paddingHorizontal: 20
  },
  lastButton: {
    marginTop: 10
  },
  signIn: {
    alignItems: 'center',
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    width: '100%'
  },
  textInput: {
    color: '#05375a',
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10
  },

  textSign: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18
  },
  text_header: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }
});

const SignInScreen = ({ navigation }) => {
  const { colors } = useTheme();

  const { signIn } = React.useContext(AuthContext);
  const [loginMutation] = useMutation(LOGIN);

  const goToRegister = useCallback(() => {
    navigation.navigate('Register');
  }, []);

  const loginHandle = useCallback(
    async (data: SubmitData) => {
      try {
        const result = await loginMutation({
          variables: { email: data.username, password: data.password }
        });
        console.log({ result });
        if (result.data.login) {
          await signIn(result.data.login.token);
        }
        console.log(result.data.login.token);
      } catch (e) {
        Alert.alert('Error!', 'Nombre de usuario o contraseñas incorrectos.', [{ text: 'OK' }]);
      }
    },
    [loginMutation, signIn]
  );

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={Colors.primaryColor} barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>{'Caja de Jubilaciones y pensiones de E.R.'}</Text>
      </View>
      <Animatable.View
        animation="fadeInUpBig"
        style={[
          styles.footer,
          {
            backgroundColor: colors.background
          }
        ]}
      >
        <LoginForm onSubmit={loginHandle} />

        <Button type="outline" style={[styles.button, styles.lastButton]} onPress={goToRegister}>
          {'Registrarse'}
        </Button>
      </Animatable.View>
    </View>
  );
};

export default SignInScreen;
