import { gql, useMutation, useQuery } from '@apollo/client';
import { useRoute } from '@react-navigation/native';
import moment from 'moment';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { Animated, StyleSheet, Share, Alert } from 'react-native';
import { View } from 'react-native-animatable';
import { Snackbar } from 'react-native-paper';
import QRCode from 'react-native-qrcode-svg';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import 'moment/locale/es-mx';

import Button from '~/components/Button';
import Timeline from '~/components/Timeline';
import BodyMedium from '~/components/typography/body-medium';
import Colors from '~/constants/Colors';
import useColorScheme from '~/hooks/useColorScheme';

const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 30;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const ADD_TO_FAVORITES = gql`
  mutation addToDocumentFavorites($document_id: Int!) {
    addToDocumentFavorites(document_id: $document_id)
  }
`;

const REMOVE_FROM_FAVORITES = gql`
  mutation removeFromDocumentFavorites($document_id: Int!) {
    removeFromDocumentFavorites(document_id: $document_id)
  }
`;

const FAVORITES_DOCUMENTS = gql`
  query {
    myInfo {
      favorite_documents {
        ids
        documents {
          id
          applicant
          applicants {
            first_name
            last_name
          }
        }
      }
    }
  }
`;

const styles = StyleSheet.create({
  actions: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: HEADER_MIN_HEIGHT
  },
  content: {
    paddingTop: HEADER_MAX_HEIGHT + 30,
    paddingHorizontal: 10
  },
  container: {
    overflow: 'visible',

    flex: 1
  },
  firstButton: {
    marginRight: 20
  },
  header: {
    height: HEADER_MAX_HEIGHT,
    left: 0,
    overflow: 'hidden',
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1
  },
  list: {
    flex: 1
  },
  listItem: {
    marginTop: 10,
    paddingVertical: 5
  },
  qrcode: {
    alignItems: 'center',
    height: 100,
    position: 'absolute',
    top: HEADER_MAX_HEIGHT - 50,
    left: 0,
    right: 0,
    justifyContent: 'center',
    zIndex: 2
  },
  safeArea: {
    flex: 1
  }
});

const QRCODE_BASE_URL = 'http://expedientes.cajajper.gov.ar/expedientes/view/';

const ExpedientesViewScreen = ({ navigation }): React.ReactElement => {
  const colorScheme = useColorScheme();
  const [snackBarVisible, setSnackBarVisible] = React.useState(false);
  const [snackBarMessage, setSnackBarMessage] = React.useState('');

  const [isFavorite, setIsFavorite] = React.useState(false);

  const onDismissSnackBar = useCallback(() => setSnackBarVisible(false), []);

  const scroll = useRef(new Animated.Value(0)).current;
  const ref = useRef(null);

  const { data, loading } = useQuery(FAVORITES_DOCUMENTS,{
    fetchPolicy: 'network-only'
  });

  const [addToFavorites] = useMutation(ADD_TO_FAVORITES);
  const [removeFromFavorites] = useMutation(REMOVE_FROM_FAVORITES);

  //  const scrollYClamped = Animated.diffClamp(scroll, 0, HEADER_MAX_HEIGHT);

  const translateHeader = scroll.interpolate({
    extrapolate: 'clamp',
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE]
  });

  const scaleQrCode = scroll.interpolate({
    extrapolate: 'clamp',
    inputRange: [-HEADER_MAX_HEIGHT, 0, HEADER_MAX_HEIGHT],
    outputRange: [2, 1, 0.58]
  });

  const route = useRoute();

  const { item } = route.params;
  if (!item) {
    navigation.navigate('home');
  }

  const getSolicitante = useMemo(() => {
    if (item.applicants.length >= 1) {
      const fullName = `${item.applicants[0].first_name.trim()} ${item.applicants[0].last_name.trim()}`.trim();

      if (fullName.length === 0) {
        return 'S/N';
      }

      return fullName;
    }

    const ret = item.applicant ? item.applicant.trim() : '';

    if (ret.length === 0) {
      return 'S/N';
    }

    return ret;
  }, [item]);

  const getTipoBeneficio = useMemo(() => {
    if (item.benefit_type) {
      return item.benefit_type.name;
    }
  }, [item.benefit_type]);

  const lastMovementTimelineData = item.last_movements.map((item, index) => ({
    icon: {
      content: index === 0 ? 'calendar-outline' : 'arrow-up-outline',
      style: {
        backgroundColor: Colors.light.loadingBackgroundColor,
        color: Colors.text
      }
    },
    time: {
      content: moment(item.modified).format('ll')
    },
    title: {
      content: item.office.name
    }
  }));

  const share = useCallback(() => {
    Share.share({
      message: QRCODE_BASE_URL + item.id,
      title: 'Caja de Jubilaciones y Pensiones de Entre Ríos'
    });
  }, [item.id]);

  const onFavoritePress = useCallback(async () => {
    const result = await addToFavorites({
      variables: {
        document_id: parseInt(item.id, 10)
      }
    });
    if (result.data.addToDocumentFavorites) {
      setSnackBarMessage('Expediente agregado a favoritos');
      setSnackBarVisible(true);
      setIsFavorite(true);
    }
  }, [addToFavorites, item.id]);

  const onFavoriteRemovePress = useCallback(async () => {
    const result = await removeFromFavorites({
      variables: {
        document_id: parseInt(item.id, 10)
      }
    });
    if (result.data.removeFromDocumentFavorites) {
      setSnackBarMessage('Expediente quitado de favoritos');
      setSnackBarVisible(true);
      setIsFavorite(false);
    }
  }, [removeFromFavorites, item.id]);

  const onScroll = Animated.event([{ nativeEvent: { contentOffset: { y: scroll } } }], {
    useNativeDriver: true
  });

  useEffect(() => {
    if (loading) {
      return;
    }
    setIsFavorite(data.myInfo.favorite_documents.ids.includes(parseInt(item.id, 10)));
  }, [data, item.id, loading]);

  const timelineHeader = (
    <View>
      <View style={styles.list} animation="fadeIn" delay={0}>
        <View style={styles.listItem}>
          <BodyMedium fontWeight="bold">{'NUC'}</BodyMedium>
        </View>
        <View>
          <BodyMedium>{item.id}</BodyMedium>
        </View>
        <View style={styles.listItem}>
          <BodyMedium fontWeight="bold">{'SOLICITANTE'}</BodyMedium>
        </View>
        <View>
          <BodyMedium>{getSolicitante}</BodyMedium>
        </View>
        <View style={styles.listItem}>
          <BodyMedium fontWeight="bold">{'TRAMITE INICIADO'}</BodyMedium>
        </View>
        <View>
          <BodyMedium>{getTipoBeneficio}</BodyMedium>
        </View>
        <View style={styles.listItem}>
          <BodyMedium fontWeight="bold">{'ULTIMOS MOVIMIENTOS'}</BodyMedium>
        </View>
      </View>
    </View>
  );

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.header,
          {
            transform: [{ translateY: translateHeader }]
          },
          { backgroundColor: Colors[colorScheme].primaryColor }
        ]}
      >
        <View animation="fadeIn" style={[styles.actions]}>
          {isFavorite ? (
            <Button
              small={true}
              icon={'star-outline'}
              type={'secondary'}
              style={styles.firstButton}
              onPress={onFavoriteRemovePress}
            >
              {'Quitar'}
            </Button>
          ) : (
            <Button
              small={true}
              icon={'star-outline'}
              style={styles.firstButton}
              onPress={onFavoritePress}
            >
              {'A favoritos'}
            </Button>
          )}

          <Button small={true} icon={'share-outline'} type="contrast" onPress={share}>
            {'Compartir'}
          </Button>
        </View>
      </Animated.View>

      <Animated.View
        style={[
          styles.qrcode,
          { transform: [{ translateY: translateHeader }, { scale: scaleQrCode }] }
        ]}
      >
        <QRCode value={QRCODE_BASE_URL + item.id} size={100} />
      </Animated.View>

      <Timeline
        contentContainerStyle={[styles.content]}
        ref={ref}
        data={lastMovementTimelineData}
        scrollEventThrottle={16}
        TimelineHeader={timelineHeader}
        onScroll={onScroll}
      />
      <Snackbar
        visible={snackBarVisible}
        onDismiss={onDismissSnackBar}
        action={{
          accessibilityLabel: 'Ocultar',
          label: 'Ocultar',
          onPress: onDismissSnackBar
        }}
      >
        {snackBarMessage}
      </Snackbar>
    </View>
  );
};

export default ExpedientesViewScreen;
