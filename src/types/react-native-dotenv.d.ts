declare module 'react-native-dotenv' {
  export const DEV: boolean;
  export const API_URL: string;
  export const API_URL_ANDROID: string;
  export const API_URL_IOS: string;
  export const STATIC_FILES_URL: string;
  export const STATIC_FILES_URL_IOS: string;
  export const STATIC_FILES_URL_ANDROID: string;
}
