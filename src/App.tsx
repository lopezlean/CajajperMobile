import { ApolloProvider } from '@apollo/client';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as SecureStore from 'expo-secure-store';
import React, { useRef } from 'react';
import { StatusBar } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import { RecoilRoot } from 'recoil';

import { Text } from './components/Text';
import Theme, { darkTheme, lightTheme } from './constants/Theme';
import AuthContext from './context/AuthContext';
import client from './graphql/client';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false
  })
});

const App = (): React.ReactElement => {
  const isLoadingResourcesComplete = useCachedResources();
  const colorScheme = useColorScheme();

  const notificationListener = useRef();
  const responseListener = useRef();

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null
    }
  );

  React.useEffect(() => {
    //registerForPushNotificationsAsync().then((token) => console.log({ token }));

    notificationListener.current = Notifications.addNotificationReceivedListener((notification) => {
      //setNotification(notification);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener((response) => {
      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;

      try {
        const isAvailableAsync = await SecureStore.isAvailableAsync();
        console.log({ isAvailableAsync });
        userToken = await SecureStore.getItemAsync('userToken');
        console.log(2);
        console.log({ userToken });
      } catch (e) {
        // Restoring token failed
        console.log({ e });
      }

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ token: userToken, type: 'RESTORE_TOKEN' });
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (token: string) => {
        try {
          await SecureStore.setItemAsync('userToken', token);
          dispatch({ token, type: 'SIGN_IN' });
        } catch (e) {
          // Restoring token failed
          console.log({ e });
        }
      },
      signOut: async () => {
        try {
          await SecureStore.deleteItemAsync('userToken');
          dispatch({ type: 'SIGN_OUT' });
        } catch (e) {
          // Restoring token failed
          console.log({ e });
        }
      },
      signUp: async (token: string) => {
        try {
          await SecureStore.setItemAsync('userToken', token);
          dispatch({ token, type: 'SIGN_IN' });
        } catch (e) {
          // Restoring token failed
          console.log({ e });
        }
      }
    }),
    []
  );
  if (!isLoadingResourcesComplete || state.isLoading) {
    /* TODO: add splash screen */
    return <Text>{'Iniciando...'}</Text>;
  }

  return (
    <AuthContext.Provider value={authContext}>
      <SafeAreaProvider>
        <PaperProvider
          settings={{
            icon: (props) => <Icon {...props} />
          }}
          theme={colorScheme === 'light' ? lightTheme : darkTheme}
        >
          <ApolloProvider client={client}>
            <RecoilRoot>
              <Navigation colorScheme={colorScheme} userToken={state.userToken} />
              <StatusBar />
            </RecoilRoot>
          </ApolloProvider>
        </PaperProvider>
      </SafeAreaProvider>
    </AuthContext.Provider>
  );
};

export default App;
