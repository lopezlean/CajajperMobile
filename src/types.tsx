export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Home: undefined;
  Documents: undefined;
};

export type HomeParamList = {
  HomeScreen: undefined;
  NewsViewScreen: undefined;
};

export type DocumentsParamList = {
  DocumentsScreen: undefined;
  DocumentViewScreen: undefined;
};
