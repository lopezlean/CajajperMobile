module.exports = {
  '*.md': ['prettier --write'],
  '*.{js,jsx,ts,tsx}': ['npm run fix:lint:js']
};
